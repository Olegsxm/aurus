import { Observable } from 'rxjs/Observable';
import { Injectable, EventEmitter } from '@angular/core';
import {
  Http,
  RequestMethod,
  RequestOptionsArgs,
  RequestOptions,
  Request,
  Response,
  ConnectionBackend,
  Headers,
  ResponseContentType
} from '@angular/http';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { Router } from '@angular/router';
import * as _ from "lodash";
import {environment} from "../../environments/environment";

@Injectable()
export class JsonHttp extends Http {
  private static logout: EventEmitter<any> = new EventEmitter();
  private adminPath = '/admin';

  private baseApiUrl = environment.config.apiUrl;

  private getRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
    options = new RequestOptions({
      withCredentials: true,
      responseType: ResponseContentType.Json
    }).merge(options);
    let headers = new Headers(options.headers);
    if (!headers.get('Content-Type')) {
      headers.set('Content-Type', 'application/json;charset=UTF-8');
    }
    let tokenRequired = (options.method !== RequestMethod.Get
      && options.method !== RequestMethod.Head
      && options.method !== RequestMethod.Options);
    if (tokenRequired) {
      let xsrfToken = this.cookies.get('XSRF-TOKEN');
      if (xsrfToken) {
        //console.log('Use token header...');
        headers.set('X-XSRF-TOKEN', xsrfToken);
      }
    }
    options.headers = headers;

    console.log('Options with creidentals: ', options.withCredentials);
    return options;
  }

  private getUrl(url: string): string {
    console.warn('get url: ', url);
    if (url.startsWith('http')) {
      return url;
    }
    if (url.startsWith('/')) {
      return this.baseApiUrl + url;
    }
    return this.baseApiUrl + this.adminPath + '/' + url;
  }

  private intercept(observable: Observable<Response>): Observable<Response> {
    return observable.catch((err, source) => {
      console.log('http error intercepted: ', err);
      if (err.status === 401 && !_.endsWith(err.url, '/user')) {
        this.logoutEmitter.emit(null);
        return Observable.empty();
      } else {
        return Observable.throw(err);
      }
    });
  }

  constructor(backend: ConnectionBackend,
    defaultOptions: RequestOptions,
    private cookies: CookieService,
    private router: Router) {
    super(backend, defaultOptions);
    console.log(this.cookies);
  }

  get logoutEmitter() {
    return JsonHttp.logout;
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    if (url instanceof String) {
      return this.intercept(super.request(this.getUrl(url), options));
    }
    return this.intercept(super.request(url, options));
  }

  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(super.get(this.getUrl(url), this.getRequestOptionArgs(options)));
  }

  head(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(super.head(this.getUrl(url), this.getRequestOptionArgs(options)));
  }

  post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(super.post(this.getUrl(url), body, this.getRequestOptionArgs(options)));
  }

  put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(super.put(this.getUrl(url), body, this.getRequestOptionArgs(options)));
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(super.delete(this.getUrl(url), this.getRequestOptionArgs(options)));
  }

  patch(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(super.patch(this.getUrl(url), body, this.getRequestOptionArgs(options)));
  }
}
