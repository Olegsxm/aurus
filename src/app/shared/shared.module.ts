import { NgModule, SkipSelf, Optional } from '@angular/core';
import { CommonModule }   from '@angular/common';
import { PermissionsService,
         PermissionsResolver } from './permissions.service';
import { AuthService }      from './auth.service';
import { AuthGuard } from './auth-guard.service';
import { PagerService } from './pager.service';
import { XHRBackend, RequestOptions, HttpModule } from '@angular/http';
import { JsonHttp } from './json-http';
import { PagerComponent } from './pager.component';
import { CanDeactivateGuard } from './can-deactivate-guard.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { Router } from '@angular/router';
import { AError, Errors } from './error.message';

export function httpFactory(xhrBackend: XHRBackend,
    requestOptions: RequestOptions,
    cookies: CookieService,
    router: Router,
    authService: AuthService) {
  return new JsonHttp(xhrBackend, requestOptions, cookies, router);
}

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
  ],
  exports: [
    PagerComponent,
  ],
  declarations: [
    PagerComponent,
  ],
  providers: [
    CookieService,
    {
      provide: JsonHttp,
      useFactory: httpFactory,
      deps: [XHRBackend, RequestOptions, CookieService, Router]
    },
    PermissionsService,

    AuthService,
    AuthGuard,
    PermissionsResolver,
    PagerService,
    CanDeactivateGuard
  ]
})

export class SharedModule {
  constructor(@Optional() @SkipSelf() parentModule: SharedModule) {
    if (parentModule) {
      throw new Error(
        'SharedModule is already loaded. Import it in the AppModule only');
    }
  }
}

export {
  AuthService, AuthGuard, CanDeactivateGuard, PermissionsResolver,
  JsonHttp, PagerComponent, AError, Errors
}
