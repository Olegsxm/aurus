import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';

export interface IPage {
    label: string;
    value: any;
};

@Component({
  selector: 'app-pagination',
  templateUrl: './pager.component.html',
})

export class PagerComponent implements OnInit, OnDestroy {
  componentName: 'PagerComponent';

  private _directionLinks: boolean = true;
  private _autoHide: boolean = false;

  @Input() data;
  @Input() maxSize: number = 7;
  @Input()
  get directionLinks(): boolean {
      return this._directionLinks;
  }
  set directionLinks(value: boolean) {
      this._directionLinks = !!value && <any>value !== 'false';
  }
  @Input()
  get autoHide(): boolean {
      return this._autoHide;
  }
  set autoHide(value: boolean) {
      this._autoHide = !!value && <any>value !== 'false';
  }
  @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();

  pages: IPage[] = [];
  currentPage: number;
  itemsPerPage: number;
  totalItems: number;

  constructor() {
  }

  ngOnInit() {
    if (!this.data) {
      this.currentPage = 1;
      this.itemsPerPage = 1;
      this.totalItems = 0;
    } else {
      this.currentPage = this.data.currentPage;
      this.itemsPerPage = this.data.itemsPerPage;
      this.totalItems = this.data.totalItems;
    }
    this.updatePages();
  }

  ngOnDestroy() {
  }

  private updatePages() {
    this.pages = this.createPageArray(this.currentPage, this.itemsPerPage, this.totalItems, this.maxSize);
  }

  previous() {
    this.setCurrent(this.getCurrent() - 1);
  }

  next() {
    this.setCurrent(this.getCurrent() + 1);
  }

  isFirstPage(): boolean {
    return this.getCurrent() === 1;
  }

  isLastPage(): boolean {
    return this.getLastPage() === this.getCurrent();
  }

  setCurrent(page: number) {
    this.pageChange.emit(page);
  }

  getCurrent(): number {
    return this.currentPage;
  }

  getLastPage(): number {
    if (this.totalItems < 1) {
      return 1;
    }
    return Math.ceil(this.totalItems / this.itemsPerPage);
  }

  private createPageArray(currentPage: number, itemsPerPage: number, totalItems: number, paginationRange: number): IPage[] {
    // paginationRange could be a string if passed from attribute, so cast to number.
    paginationRange = +paginationRange;
    let pages = new Array<IPage>();
    const totalPages = Math.ceil(totalItems || 1 / itemsPerPage);
    const halfWay = Math.ceil(paginationRange / 2);

    const isStart = currentPage <= halfWay;
    const isEnd = totalPages - halfWay < currentPage;
    const isMiddle = !isStart && !isEnd;

    let ellipsesNeeded = paginationRange < totalPages;
    let i = 1;

    while (i <= totalPages && i <= paginationRange) {
      let label;
      let pageNumber = this.calculatePageNumber(i, currentPage, paginationRange, totalPages);
      let openingEllipsesNeeded = (i === 2 && (isMiddle || isEnd));
      let closingEllipsesNeeded = (i === paginationRange - 1 && (isMiddle || isStart));
      if (ellipsesNeeded && (openingEllipsesNeeded || closingEllipsesNeeded)) {
        label = '...';
      } else {
        label = pageNumber;
      }
      pages.push(<IPage>new Object({
        label: label,
        value: pageNumber
      }));
      i++;
    }
    return pages;
  }

  private calculatePageNumber(i: number, currentPage: number, paginationRange: number, totalPages: number) {
    let halfWay = Math.ceil(paginationRange / 2);
    if (i === paginationRange) {
      return totalPages;
    } else if (i === 1) {
      return i;
    } else if (paginationRange < totalPages) {
      if (totalPages - halfWay < currentPage) {
        return totalPages - paginationRange + i;
      } else if (halfWay < currentPage) {
        return currentPage - halfWay + i;
      } else {
        return i;
      }
    } else {
      return i;
    }
  }

}
