import * as _ from 'lodash';

export class PagerService {
  createPager(totalItems: number, currentPage = 1, pageSize = 10) {
    return {
        totalItems: totalItems,
        currentPage: currentPage,
        pageSize: pageSize
    };
  }
}
