import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

import { Headers, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Router, NavigationExtras } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/do';
import { JsonHttp } from '../shared/json-http';
import * as _ from "lodash";

export interface User {
  name: string;
  roles: string[];
  companyId: string;
}

@Injectable()
export class AuthService {
  private user: User;
  redirectUrl: string;

  private checkUrl = 'user';
  private loginUrl = 'user';
  private logoutUrl = '/logout.json';

  constructor(private http: JsonHttp,
    private router: Router) {
    this.user = null;

    http.logoutEmitter.subscribe(() => {
      this.cleanup();
    });
  }

  get loggedIn(): boolean {
    return this.user != null;
  }

  get companyUrl(): string {
    if (this.loggedIn) {
      return 'companies/' + this.user.companyId;
    }
    return '';
  }

  private handleAuth(aResponse) {
    console.log('auth response: ', aResponse);
    if (aResponse && aResponse.authenticated) {
      let roles: string[] = [];
      _.forEach(aResponse.authorities, function (val) {
        roles.push(val.authority);
      });
      this.user = <User> {
        name: aResponse.name,
        roles: roles,
        companyId: aResponse.principal.companyId
      };
    } else {
      this.user = null;
      console.error('Failed to authenticate user', aResponse);
    }
  }

  restoreLogin() {
    let loginState = <Subject<boolean>>new Subject();
    this.http.get(this.checkUrl)
      .map(result => {
        console.log(result);
        return result.json();
      })
      .subscribe(result => {
        this.handleAuth(result);
        loginState.next(this.loggedIn);
      },
      error => {
        loginState.next(false);
      });
    return loginState.asObservable();
  }

  private cleanup() {
    this.user = null;
    this.router.navigate(['/login']);
  }

  login(username: string, password: string) {
    if (this.loggedIn) {
      return Observable.of('');
    }

    let loginError: Subject<string> = new Subject<string>();
    let headers = new Headers({ 'authorization': 'Basic ' + btoa(username + ':' + password) });
    //let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({
      headers: headers,
      withCredentials: true
    });

    this.http.get(this.loginUrl, options)
      .map((res: Response) => res.json())
      .subscribe(
        result => {
          this.handleAuth(result);
          if (this.loggedIn) {
            loginError.next('');
            let redirect = this.redirectUrl ? this.redirectUrl : '';
            let navigationExtras: NavigationExtras = {
              preserveQueryParams: true,
              preserveFragment: true
            };
            this.router.navigate([redirect], navigationExtras);
          } else {
            loginError.error('Failed to log in');
          }
        },
        err => {
          loginError.error('Failed to log in');
        }
      );
    return loginError.asObservable();
  }

  logout(): void {
    if (!this.loggedIn) {
      return;
    }

    this.http.post(this.logoutUrl, null)
      .map(result => result.json())
      .subscribe(r => this.cleanup(), e => this.cleanup());
  }

  getBrowserLanguage(){
    return this.http.get('/language').map(res => res.json())
  }
}
