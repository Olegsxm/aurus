import { Response } from '@angular/http';

export interface IError {
  id: string;
  message?: string;
}

 export const Errors = Object.freeze({
  UnknownError: 'unknown_error',
 });

export class AError implements IError {
  static parseErrorResponse(response: Response): IError {
    if (response.status === 0) {}
      return new AError(Errors.UnknownError, 'Unknoen server errror');
  }

  constructor(public id: string,
    public message?: string) {
  }

}
