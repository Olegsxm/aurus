import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import { JsonHttp } from '../shared/json-http';


import * as _ from 'lodash';

import 'rxjs/add/operator/map';

export interface Permission {
  id: string;
  title: string;
}

export function permissionTitle(aType: string): string {
  let result = '';

  switch (aType) {
    case 'SHARE_MOUSE':
      result = 'Send mouse events';
      break;
    case 'SHARE_WEB_CAMERA':
      result = 'Enable web camera translation';
      break;
    case 'COBROWSING':
      result = 'Enable corowser';
      break;
    case 'SHARE_DESKTOP':
      result = 'Enable desktop sharing';
      break;
    case 'SEND_SCREENSHOT':
      result = 'Enable send screenshots';
      break;
    case 'SEND_FILE':
      result = 'Enable file transfer';
      break;
    case 'SEND_TEXT':
      result = 'Enable sending text messages';
      break;
    default:
      break;
  }
  return result;
};

@Injectable()
export class PermissionsService {
  private allPermissions: Array<Permission> = [];
  private apiUrl = 'permissions';


  constructor(private http: JsonHttp) {
  }

  get permissions(): Array<Permission> {
    return this.allPermissions;
  }

  set permissions(aPerms: Array<Permission>) {
    this.allPermissions = aPerms;
  }

  permission(id: string): Permission {
    if (this.allPermissions[id] != null) {
      return _.cloneDeep(this.allPermissions[id]);
    }
    return null;
  }

  premissionsListFromResponse(aResponse: Response): string[] {
    let result: string[] = [];
    try {
      _.forEach(aResponse.json()._embedded.permissions, function (aItem) {
        result.push(aItem._links.self.href);
      });
    } catch (e) {
      console.error(e);
    }
    return result;
  }

  premissionsFromResponse(aResponse: Response): Permission[] {
    let result: Permission[] = [];
    try {
      _.forEach(aResponse.json()._embedded.permissions, function (aItem) {
        let p: Permission;
        p = <Permission>new Object({
          id: aItem._links.self.href,
          title: permissionTitle(aItem.type)
        });
        result.push(p);
      });
    } catch (e) {
    }
    return result;
  }

  load(): Observable<any> {
    return Observable.create(observer => {
      if (this.allPermissions.length > 0) {
        observer.next(this.allPermissions);
        observer.complete();
      }
      this.http.get(this.apiUrl)
        .map(response => this.premissionsFromResponse(response))
        .share()
        .subscribe(permissions => {
          this.allPermissions = permissions;
          observer.next(this.allPermissions);
          observer.complete();
        }, error => {
          console.log('Can\'t load permissions...');
          observer.next([]);
          observer.complete();
        });
    });
  }
}

@Injectable()
export class PermissionsResolver implements Resolve<Array<Permission>> {
  constructor(private service: PermissionsService) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.service.load();
  }
}
