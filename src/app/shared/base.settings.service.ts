import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Response } from '@angular/http';
import { JsonHttp } from '../shared/json-http';
import { AuthService } from '../shared/auth.service';
import { Observable } from 'rxjs';
import { AError } from '../shared';
import * as _ from "lodash";

export interface SettingInfo {
  key: string;
  convertor: any;
  url: string;
}

interface Setting {
  key: string;
  value: string;
}

@Injectable()
export class BaseSettingsService {
  private data: any;
  private subjectData: Subject<any>;

  static numberFromString(str: string): number {
    return +str;
  }

  static boolFromString(str: string): boolean {
    return str === 'true';
  }

  static stringFromString(str: string): string {
    return str;
  }

  private mapResponse(response: Response) {
    let result = _.cloneDeep(this.data);
    try {
      let map = this.settingsMap;
      let settings = response.json()._embedded.companySettings;
      _.forEach(settings, function (aItem) {
        let info = <SettingInfo>map[aItem.key];
        if (info && result.hasOwnProperty(info.key)) {
          result[info.key] = info.convertor(aItem.value);
          info.url = aItem._links.self.href;
        }
      });
    } catch (e) {
      console.error(e);
    }
    return result;
  }

  constructor(private apiUrl: string,
    private settingsMap: any,
    private http: JsonHttp,
    private authService: AuthService) {
    this.data = this.defaults();
    this.subjectData = new Subject<any>();
  }

  private updateData(aData) {
    this.data = aData;
    this.subjectData.next(this.data);
  }

  get settings() {
    return this.subjectData;
  }

  defaults(): any {
    return {};
  }

  get(aUrl?: string) {
    console.log('load settings from: ', aUrl);
    let sbo = this.http.get(aUrl ? aUrl : this.apiUrl)
      .map(response => this.mapResponse(response))
      .share();

    sbo.subscribe(s => {
      this.updateData(s);
    }, error => {
      this.updateData(this.defaults());
    });
    return sbo;
  }

  set(aSettings: any, aUrl?: string) {
    let subject = new Subject<any>();
    let self = this;
    let updatedSettings = _.cloneDeep(this.data);
    let observableBatch = [];
    let http = this.http;

    _.forEach(this.settingsMap, function (aValue, aKey) {
      let sKey = aValue.key;
      if (self.data[sKey] !== aSettings[sKey]) {
        if (!aValue.url) {
          if (aUrl) {
            console.log('create setting: ', aUrl);
            observableBatch.push(http.post(aUrl, JSON.stringify({value: aSettings[sKey]}))
              .map((res: Response) => <Setting>res.json()).share());
          }
        } else {
          console.log('set setting: ', aValue.url);
          observableBatch.push(http.patch(aValue.url, JSON.stringify({value: aSettings[sKey]}))
            .map((res: Response) => <Setting>res.json()).share());
        }
      }
    });

    if (observableBatch.length === 0) {
      subject.next(updatedSettings);
    }

    Observable.forkJoin(observableBatch).share()
      .subscribe((results: Setting[]) => {
        console.warn(results)
        let map = this.settingsMap;
        _.forEach(results, function (aReult) {
          let info = <SettingInfo>map[aReult.key];
          updatedSettings[info.key] = info.convertor(aReult.value);
        });
        console.log('Update data: ', updatedSettings);
        this.updateData(updatedSettings);
      },
      error => {
        subject.error(AError.parseErrorResponse(error));
      });
    return subject.asObservable();
  }
}
