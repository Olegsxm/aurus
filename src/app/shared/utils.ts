import * as _ from "lodash";

export class Utils {
  static getOnlyChanged(aOrig: any, aData: any) {
    let result = {};
    _.forEach(aOrig, function (aOrigValue, aKey) {
      let value = aData[aKey];
      if (!_.isEqual(value, aOrigValue)) {
        if (!_.isArray(value) && _.isObject(value)) {
          let tmp = Utils.getOnlyChanged(aOrigValue, value);
          if (!_.isEmpty(tmp)) {
            result[aKey] = tmp;
          }
        } else {
          result[aKey] = aData[aKey];
        }
      }
    });
    return result;
  }
}
