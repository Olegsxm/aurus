import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-error-popup',
  templateUrl: './error-popup.component.html',
  styleUrls: ['./error-popup.component.scss']
})
export class ErrorPopupComponent implements OnInit {

  constructor() { }
  private dispayErrorPopup:boolean = false;


  ngOnInit() {
  }


  hideErrorPopup(){
    this.dispayErrorPopup = false;
  }

  showErrorPopup(){
    this.dispayErrorPopup = true;
  }
}
