import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {

  constructor() { }

  @Input('list') list;

  @Output() select = new EventEmitter();

  toggle: boolean = true;

  first;

  ngOnInit(){
    this.first = this.list[0];
  }

  onSelect(item){
    this.first = item;
    this.select.emit(item);
    this.toggle = false;
  }
}
