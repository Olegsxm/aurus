import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-confirm-window',
  templateUrl: './confirm-window.component.html',
  styleUrls: ['./confirm-window.component.scss']
})
export class ConfirmWindowComponent implements OnInit {

  @Input('title') title;
  @Output() event = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  cancel(){
    this.event.emit(false);
  }


  delete(){
    this.event.emit(true);
  }

}
