import {Component, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TopicsService, Topic } from '../services/topics.service';
import * as _ from "lodash";
import {ErrorPopupComponent} from "../../../shared-components/error-popup/error-popup.component";
@Component({
  selector: 'app-topics-add',
  templateUrl: './topics-add.component.html',
  styleUrls: ['./topics-add.component.scss']
})
export class TopicsAddComponent implements OnInit {
  public form: FormGroup;

  @ViewChild(ErrorPopupComponent) errorPopup: ErrorPopupComponent;

  constructor(private service: TopicsService,
              private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      description: ['', [Validators.required, Validators.minLength(5)]]
    });
  }

  save() {
    if(this.form.invalid) return;

    this.service.create(this.form.value)
      .subscribe(result => {
          this.router.navigate(['/topics'/*, this.service.idFromUrl(result.id)*/]);
        },
        error => {
          console.error('Failed to add menu: ', error);
          this.errorPopup.showErrorPopup();
        }
      );

  }
}
