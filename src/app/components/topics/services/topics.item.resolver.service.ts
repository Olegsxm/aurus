import { Injectable }             from '@angular/core';
import { Router, Resolve,
         ActivatedRouteSnapshot } from '@angular/router';

import { Observable } from 'rxjs';
import { Topic, TopicsService } from './topics.service';

@Injectable()
export class TopicsItemResolver implements Resolve<Topic> {
  public topic: Topic;

  constructor(private service: TopicsService,
              private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
    let id = route.params['id'];
    return Observable.create(subject => {
      this.service.get(this.service.urlFromId(id)).subscribe(data => {
        subject.next(data);
        subject.complete();
      });
    });
  }
}
