import { Injectable } from '@angular/core';
import { JsonHttp } from '../../../shared/json-http';
import { URLSearchParams } from '@angular/http';
import { Subject, Observable } from 'rxjs';
import { AuthService } from '../../../shared/auth.service';
import * as _ from 'lodash';

export interface Topic {
  id?: string;
  description: string;
  checkCaptcha: boolean;
  main: boolean;
}

function topicFromResponseData(aData: any): Topic {
  return <Topic>{
    id: aData._links.self.href,
    description: aData.description,
    checkCaptcha: aData.checkCaptcha,
    main: aData.checkCaptcha
  }
}

@Injectable()
export class TopicsService {
  private apiUrl = 'menus';
  private _page: any;
  private pageData: Subject<any>;
  private observable: Observable<any>;

  constructor(private http: JsonHttp,
    private authService: AuthService) {
    this.pageData = <Subject<any>>new Subject();
    this.observable = this.pageData.asObservable();
  }

  load(aPage: number) {
    let params = new URLSearchParams();
    params.set('page', '' + aPage);
    this.http.get(this.apiUrl, params)
      .map(response => {
        let data = response.json();
        if(typeof data._embedded == "undefined"){
          data._embedded = {menus:[]}
        }
        let result = {
          list: [],
          page: {
            totalItems: data.page.totalElements,
            itemsPerPage: data.page.size,
            currentPage: aPage
          }
        };
        _.forEach(data._embedded.menus, function (aTopic) {
          result.list.push(topicFromResponseData(aTopic));
        });
        return result;
      })
      .share()
      .subscribe(page => {
        console.log('Got menus: ', page);
        this._page = page;
        this.pageData.next(this._page);
      });
  }

  create(topic: any) {
    let data = topic;
    data.company = this.authService.companyUrl;
    return this.http.post(this.apiUrl, JSON.stringify(data))
      .map(response => topicFromResponseData(response.json()))
      .share();
  }

  get(topicUrl: string) {
    return this.http.get(topicUrl)
      .map(response => topicFromResponseData(response.json()))
      .share();
  }

  remove(topicUrl: string) {
    return this.http.delete(topicUrl);
  }

  save(aTopic: Topic) {
    let saveObject = _.cloneDeep(aTopic);
    let url = saveObject.id;
    delete saveObject.id;
    return this.http.patch(url, JSON.stringify(saveObject))
      .map(respose => topicFromResponseData(respose.json()))
      .share();
  }

  get page(): Observable<any> {
    return this.observable;
  }

  idFromUrl(aCallcenterUrl: string) {
    let parts = aCallcenterUrl.split('/');
    return parts[parts.length - 1];
  }

  urlFromId(aCallcenterId: string) {
    return this.apiUrl + '/' + aCallcenterId;
  }

  getMenuItems(id){
    return this.http.get(`/admin/menus/${id}/menuItems`).map(res => res.json())
  }

  getCallCentersList(){
    return this.http.get("callCenters").map(res => res.json())
  }

  getCallCentersById(id){
    return this.http.get(id).map(res => res.json());
  }

  saveMenuItem(url, item){
    return this.http.patch(url, item).map(res => res.json())
  }

  addMewMenuItem(item){
    return this.http.post("menuItems", item).map(res => res.json());
  }

  deleteItem(url){
    return this.http.delete(url).map(res => res.json());
  }

  getAllTopics(){
    return this.http.get(this.apiUrl).map(res => res.json())
      .map(res => {
        if(typeof res._embedded !== "undefined"){return res._embedded.menus}
        else {return []}}
      )
  }

  changeMainAndDefaultValue(id:string,type:string, value: boolean): Observable<any>{

    if(type == 'main'){
      return this.http.patch(id, JSON.stringify({main:value})).map(res => res.json())
    }

    if(type == 'captcha'){
      return this.http.patch(id, JSON.stringify({checkCaptcha:value})).map(res => res.json())
    }
  }
}
