import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TopicsService, Topic } from '../services/topics.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Utils } from '../../../shared/utils';
import * as _ from "lodash";

@Component({
  selector: 'app-topics-edit',
  templateUrl: './topics-edit.component.html',
  styleUrls: ['./topics-edit.component.scss']
})
export class TopicsEditComponent implements OnInit {

  original: Topic;
  showEditRoot:boolean = false;

  topicName: string;

  public form: FormGroup;

  constructor(private service: TopicsService,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute,
  ) {}

  ngOnInit(){
    this.original = this.route.snapshot.data['topic'];
    this.topicName = this.original.description;

    this.form = this.formBuilder.group({
      description: [this.original.description, [Validators.required, Validators.minLength(5)]],
      main: this.original.main,
      checkCaptcha: this.original.checkCaptcha
    })
    this.form.valueChanges.subscribe(value => this.onChange(value));
    this.topicName = this.form["_value"]["description"];

    this.getMenuItems()
    this.getAllCallCenters();
  }
  private onChange(aValues) {
    this.topicName = aValues.description;
    if(this.topicName.length < 5){
      return false;
    }
    let newData = <Topic>Utils.getOnlyChanged(this.original, aValues);
    if (_.isEmpty(newData)) {
      return;
    }
    newData.id = this.original.id;
    this.service.save(newData).subscribe(response => {
      console.log('Menu stored...');
    }, error => {
      console.error('Failed to add menu: ', error);
    });
  }

  editRoot(){
    this.showEditRoot = true;
  }


  menuItemsArray: IGetMenuItem[] = []

  getMenuItems(){
    let menuId = this.route.snapshot.data["topic"].id.split('/').pop();
    this.service.getMenuItems(menuId).subscribe(res => {

      if(res._embedded.menuItems.length > 0){
        this.menuItemsArray = res._embedded.menuItems;
        console.warn(this.menuItemsArray)
      } else {
        this.menuItemsArray = [];
      }

    }, err => {})
  }

  displayEditMenuItemForm:boolean = false;

  editableMenuItem: IGetMenuItem;

  editMenuItem(item:IGetMenuItem){
    this.editableMenuItem = _.clone(item);
    this.displayEditMenuItemForm = true;
  }

  callCenters: ICallCenter[] = [];
  selectedCallCenter: ICallCenter = null;

  getAllCallCenters(){
    this.service.getCallCentersList().subscribe(res => {
      this.callCenters = res._embedded.callCenters;
    })
  }

  cancelEditMenuItem(){
    this.displayEditMenuItemForm = false;
  }

  onSelect(e){
    this.selectedCallCenter = e;
  }

  saveMenuItem(editItem){

    let callCenter = this.selectedCallCenter == null ? editItem : this.selectedCallCenter;

    let center = {
      description: editItem.description,
      dtmf: editItem.dtmf,
      number: editItem.number,
      type: editItem.type,
      menu: editItem._links.menu["href"],
      parent: editItem.parent,
      deleted: editItem.deleted,
      callCenter: callCenter._links.self["href"]
    }

    let url = editItem["_links"]["self"]["href"]

    this.service.saveMenuItem(url, center).subscribe(
      res => {console.log("Done")},

      err => {console.log("error " + err)}
    )

  }

  deleteMenuItem(item){
    let url = item["_links"]["self"]["href"];
    this.service.deleteItem(url).subscribe(
      res => {this.getMenuItems();},

      err => {console.log("err " + err)}
    );
  }



  newMenuItemCallCenter: ICallCenter;
  dispayAddMenuItemForm: boolean = false;
  newMenuItemForm:FormGroup;

  createMenuItem(e){
    e.preventDefault();
    this.showEditRoot = false;

    this.newMenuItemForm = this.formBuilder.group({
      description: ["", [Validators.required, Validators.minLength(5)]],
      dtmf: ""
    })

    this.dispayAddMenuItemForm = true;
  }



  selectNewMenuItemCallCenter(item){
    this.newMenuItemCallCenter = item;
  }

  cancelCreateNewMenuItem(){
    this.dispayAddMenuItemForm = false;
    this.newMenuItemForm = null;
  }



  saveNewMenuItem(){
    let description: string = this.newMenuItemForm["_value"]["description"];

    if(description.length < 5) return false;

    let newItem: IPostMenuItem = {
      deleted: false,
      description: description,
      dtmf: this.newMenuItemForm["_value"]["dtmf"],
      number: 2,
      type: "ITEM",
      menu: this.original.id,
      parent: null,
      callCenter: this.newMenuItemCallCenter == undefined ? this.callCenters[0]._links.self['href'] : this.newMenuItemCallCenter._links.self['href']
    }

    this.service.addMewMenuItem(newItem).subscribe(res => {
      this.getMenuItems();

      this.dispayAddMenuItemForm = false;
    })
  }


}

export interface IPostMenuItem{
  description: string;
  dtmf: string;
  number: number;
  type: string;
  menu: string;
  parent: string;
  callCenter: string;
  deleted:boolean;
}

export interface IMenuItemLinks{
  callCenter: Object;
  menu: Object;
  parent: Object;
  self: Object;
}

export interface IGetMenuItem{
  links: Array<IMenuItemLinks>;
  deleted: boolean;
  description: string;
  number: number;
  type: string;
}

export interface ICallCenterLinks{
  callCenter: Object;
  company: Object;
  settings: Object;
  self:Object;
}

export interface ICallCenter{
  deleted: boolean;
  description: string;
  ipAddress: string;
  phoneNumber: string;
  phoneNumberMask: string;

  _links: ICallCenterLinks;
}

