import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TopicsService } from '../services/topics.service';

@Component({
  selector: 'app-topics-list',
  templateUrl: './topics-list.component.html',
  styleUrls: ['./topics-list.component.scss']
})
export class TopicsListComponent implements OnInit {

  public items: any[];
  public mainItem: Object = null;
  public page: any;

  setPage(aPage: number) {
    this.service.load(aPage);
  };

  add() {
    this.router.navigate(['add'], {relativeTo: this.route});
  }

  edit(item: any) {
    let id = item._links.self.href.split('/').pop();
    this.router.navigate(['edit', /*this.service.idFromUrl(item.id)*/ id], { relativeTo: this.route });
  }

  remove(item: any) {
    this.displayConfirmWindow = true;
    this.deletedItem = item;
  }

  private displayConfirmWindow: boolean = false;
  private confirmWindowText: string = 'agent';
  private deletedItem = null;

  deleteItem($event){
    if($event){
      this.service.remove(this.deletedItem._links.self.href).subscribe(
        result => {
         // this.setPage(this.page.currentPage);
          this.items.splice(this.items.indexOf(this.deletedItem), 1);
          this.deletedItem = null;
        },
        err => console.error(err),
        () => {

        }
      );
      this.displayConfirmWindow = false;
    }

    if(!$event){
      this.deletedItem = null;
      this.displayConfirmWindow = false;
    }
  }


  constructor(private service: TopicsService,
              private route: ActivatedRoute,
              private router: Router)
  {this.items = [];}

  ngOnInit(){

    this.service.getAllTopics().subscribe(
      res => this.items = res,
      err => console.error(err),
      () => {
        this.items.map(i => {if(i.main){this.mainItem = i}});
        if(this.mainItem == null && this.items.length > 0){
          this.items[0].main = true;
          this.mainItem = this.items[0];
          this.service.changeMainAndDefaultValue(this.mainItem['_links'].self.href, 'main',true)
            .subscribe();
        }
      }
    )

  }

  changeMain(item, value){
    let type = 'main';

    if(this.items.length < 2) return;

    item.main = value;

    if(value){
      //this.mainItem = item;
      this.service.changeMainAndDefaultValue(item._links.self.href, type, value)
        .subscribe(
          res => {
            if(this.mainItem !== null){
              this.service.changeMainAndDefaultValue(this.mainItem['_links'].self.href, type, false)
                .subscribe(
                  res => {
                    this.items[this.items.indexOf(this.mainItem)].main = false;
                    this.mainItem = item;
                  }
                )
            }
          }
        )
    }


  }

  changeCaptcha(item, value){
    let type = 'captcha';
    item.checkCaptcha = value;
    this.service.changeMainAndDefaultValue(item._links.self.href, type, value)
      .subscribe(
        res => {},
        err => console.error(err),
        () => {}
      )
  }


}
