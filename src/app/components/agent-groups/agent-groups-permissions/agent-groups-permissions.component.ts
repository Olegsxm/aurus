import {Component, Input, OnInit, OnChanges, Output, EventEmitter} from "@angular/core";
import {GroupPermissionsService} from "../services/group.permissions.service";

@Component({
  selector: 'app-agent-groups-permissions',
  templateUrl: './agent-groups-permissions.component.html',
  styleUrls: ['./agent-groups-permissions.component.scss'],
  providers:[GroupPermissionsService]
})
export class AgentGroupsPermissionsComponent implements OnInit {

  @Input('data') groupPermissions;
  @Input('url') groupUrl;
  @Output() changePermissionList = new EventEmitter();
  private permissionModel = [];

  private permissionsList;

  constructor(
    private service: GroupPermissionsService
  ){}

  ngOnChanges(){

  }

  ngOnInit(){

    this.service.getAllPermission().subscribe(
      res => this.permissionsList = res,
      err => console.error(err),
      () => this.setModel()
    )
  }


  comparePerrmissions(item){
    let result = false;
    let itemUrl = item._links.self.href;

    this.groupPermissions.map(i => {
      if(i == itemUrl) result = true;
    })

    return result;
  }

  savePerrmissions($event, item){
    let itemUrl = item._links.self.href;

    if($event.target.checked){
      if(this.permissionModel.indexOf(itemUrl) == -1){
        this.permissionModel.push(itemUrl);
      }
    }

    if(!$event.target.checked){
      let index = this.permissionModel.indexOf(itemUrl);
      if(index !== -1){
        this.permissionModel.splice(index, 1)
      }
    }

    //this.service.savePermission(this.groupUrl, this.permissionModel)
    this.changePermissionList.emit(
      {
        data: this.permissionModel,
        url:this.groupUrl
      }
    );
  }

  setModel(){
    this.groupPermissions.map( i => {
      if(this.permissionModel.indexOf(i) == -1){
        this.permissionModel.push(i)
      }
    })

    this.changePermissionList.emit(
      {
        data: this.permissionModel,
        url:this.groupUrl
      }
    );
  }

}
