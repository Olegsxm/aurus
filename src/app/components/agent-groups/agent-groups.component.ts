import { Component, OnInit } from '@angular/core';
import {AgentgroupsItemResolver} from "./services/agentgroups.item.resolver.service";
import {AgentgroupsService} from "./services/agentgroups.service";

@Component({
  selector: 'app-agent-groups',
  templateUrl: './agent-groups.component.html',
  styleUrls: ['./agent-groups.component.scss'],
  // providers:[AgentgroupsItemResolver, AgentgroupsService]
})
export class AgentGroupsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
