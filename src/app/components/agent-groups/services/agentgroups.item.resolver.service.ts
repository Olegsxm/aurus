import { Injectable }             from '@angular/core';
import { Router, Resolve,
         ActivatedRouteSnapshot } from '@angular/router';

import { Observable } from 'rxjs';
import { AgentgroupsService, AgentGroup } from './agentgroups.service';

@Injectable()
export class AgentgroupsItemResolver implements Resolve<AgentGroup> {
  constructor(private service: AgentgroupsService,
              private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
    return Observable.create(subject => {
      this.service.get(this.service.urlFromId(route.params['id'])).subscribe(data => {
        subject.next(data);
        subject.complete();
      });
    });
  }
}
