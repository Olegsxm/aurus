import {Injectable} from "@angular/core";
import {JsonHttp} from "../../../shared/json-http";
import {Headers, RequestOptions} from '@angular/http';

@Injectable()

export class GroupPermissionsService{

  constructor(
    private http: JsonHttp
  ){}


  getAllPermission(){
      return this.http.get('/admin/permissions').map(res => res.json())
        .map(res => {
          return typeof res._embedded == "undefined" ? [] : res._embedded.permissions
        })
  }

  savePermission(url, data){
    let options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'text/uri-list'
      })
    });

    let permissions = '';

    data.map(i => permissions += (i + '\n'));


    return data;
    //this.http.put(url + '/permissions', permissions, options).subscribe()
  }
}
