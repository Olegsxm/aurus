import { Injectable } from '@angular/core';
import { JsonHttp } from '../../../shared/json-http';
import {URLSearchParams, Headers, RequestOptions} from '@angular/http';
import { Subject, Observable } from 'rxjs';
import { AuthService } from '../../../shared/auth.service';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { PermissionsService } from '../../../shared/permissions.service';
import * as _ from "lodash";
import {map} from "rxjs/operator/map";

export interface AgentGroup {
  id: string;
  name: string;
  basic: boolean;
  company?: string;
  permissions?: string[];
}

@Injectable()
export class AgentgroupsService {
  private apiUrl = 'agentGroups';
  private _page: any;
  private pageData: Subject<any>;
  private observable: Observable<any>;

  constructor(private http: JsonHttp,
    private authService: AuthService,
    private permissionsService: PermissionsService) {
    this.pageData = <Subject<any>>new Subject();
    this.observable = this.pageData.asObservable();
  }

  private mapSingleItem(aData: any): AgentGroup {
    return <AgentGroup>({
      id: aData._links.self.href,
      name: aData.name,
      basic: aData.basic
    });
  }


  private mapListResponse(aResponse, aPage: number) {
    let data = aResponse.json();
    let result = {
      list: [],
      page: {
        totalItems: data.page.totalElements,
        itemsPerPage: data.page.size,
        currentPage: aPage
      }
    };
    let lst = typeof data._embedded == "undefined" ? [] : data._embedded.agentGroups;
    for (let i = 0; i < lst.length; i++) {
      result.list.push(this.mapSingleItem(lst[i]));
    }
    return result;
  }

  private hadleSingleItem(aObservable) {
    let currentGroup: AgentGroup;
    return aObservable.map(response => {
      let data = response.json();
      currentGroup = this.mapSingleItem(data);
      return data._links.permissions.href;
    })
    .flatMap(permissionsUrl => this.http.get(permissionsUrl))
    .map(response => {
      currentGroup.permissions = this.permissionsService.premissionsListFromResponse(response);
      return currentGroup;
    }).share();
  }

  load(aPage: number) {
    let params = new URLSearchParams();
    return this.http.get(this.apiUrl, params)
      .map(response => this.mapListResponse(response, aPage))
      .share()
      .subscribe(page => {
        this._page = page;
        this.pageData.next(this._page);
      });
  }

  create(aItem: AgentGroup) {
    let data = _.cloneDeep(aItem);
    data.company = this.authService.companyUrl;
    return this.http.post(this.apiUrl, JSON.stringify(data))
      .share();
  }

  urlFromId(aId: string) {
    return this.apiUrl + '/' + aId;
  }

  idFromUrl(aUrl: string) {
    let parts = aUrl.split('/');
    return parts[parts.length - 1];
  }

  save(aItem: AgentGroup) {
    let saveObject = _.cloneDeep(aItem);
    let url = saveObject.id;
    delete saveObject.id;
    return this.hadleSingleItem(this.http.patch(url, JSON.stringify(saveObject)));
  }

  get(aGroupUrl: string) {
    return this.hadleSingleItem(this.http.get(aGroupUrl));
  }

  remove(aGroupUrl: string) {
    return this.http.delete(aGroupUrl).subscribe();
  }

  get page(): Observable<any> {
    return this.observable;
  }

  saveAgentGroup(data){
    let options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'text/uri-list'
      })
    });

    let $name = this.http.patch(data.id, JSON.stringify({name: data.name}));


    let permissions = '';

    data.permissions.data.map(i => permissions += (i + '\n'));

    let $permissions = this.http.put(data.permissions.url + '/permissions', permissions, options);

    return Observable.forkJoin([$name, $permissions]);
  }

  getAllPermission(){
    return this.http.get('/admin/permissions').map(res => res.json())
      .map(res => {
        return typeof res._embedded == "undefined" ? [] : res._embedded.permissions
      })
  }

  saveGroup(data){
    data.company = this.authService.companyUrl;

    let options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'text/uri-list'
      })
    });

    let permissions = '';

    data.permissions.map(i => permissions += (i + '\n'));

    return this.http.post(this.apiUrl,JSON.stringify({basic: data.basic, name: data.name, company: data.company}) )
      .map(res => res.json())
      .mergeMap(res => this.http.put(res._links.permissions.href, permissions, options))
  }
}

@Injectable()
export class AgentgroupsResolver implements Resolve<Array<AgentGroup>> {
  private groups: AgentGroup[];
  private subject: Subject<AgentGroup[]>;

  constructor(private service: AgentgroupsService) {
    this.service.page.subscribe(p => this.onPageLoad(p));
  }

  private onPageLoad(aPage: any) {
    let groups = this.groups;
    _.forEach(aPage.list, function (aItem) {
      groups.push(aItem);
    });
    if (aPage.page.totalItems <= this.groups.length) {
      this.subject.next(this.groups);
      this.subject.complete();
    } else {
      this.service.load(aPage.currentPage + 1);
    }
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    this.groups = [];
    this.subject = new Subject<AgentGroup[]>();
    this.service.load(1);
    return this.subject.asObservable();
  }
}
