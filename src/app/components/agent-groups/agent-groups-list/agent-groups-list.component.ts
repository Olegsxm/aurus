import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AgentgroupsService } from '../services/agentgroups.service';

@Component({
  selector: 'list',
  templateUrl: './agent-groups-list.component.html',
  styleUrls: ['./agent-groups-list.component.scss']
})
export class AgentgroupsListComponent implements OnInit {
  public page: any;
  public items: any[];

  constructor(private service: AgentgroupsService,
              private route: ActivatedRoute,
              private router: Router) {
    this.items = [];
  }

  ngOnInit() {
    this.service.page.subscribe(data => {
      this.items = data.list;
      this.page = data.page;
    });
    let nextPage = +this.route.snapshot.params['page'] || 1;
    this.setPage(nextPage);
  }

  add() {
    this.router.navigate(['add'], {relativeTo: this.route});
  }

  edit(item: any) {
    this.router.navigate(['edit', this.service.idFromUrl(item.id)],
      { relativeTo: this.route });
  }

  remove(item: any) {
    if(item.basic == true){return}
    this.deletedItem = item;
    this.displayConfirmWindow = true;
  }

  setPage(aPage: number) {
    this.service.load(aPage);
  };


  private displayConfirmWindow: boolean = false;
  private confirmWindowText: string = 'agent group';
  private deletedItem = null;

  deleteItem($event){
    if($event){
      this.service.remove(this.deletedItem.id);
      this.items.splice(this.items.indexOf(this.deletedItem), 1);
      this.displayConfirmWindow = false;
    }

    if(!$event){
      this.deletedItem = null;
      this.displayConfirmWindow = false;
    }
  }
}
