import {Component, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AgentgroupsService } from '../services/agentgroups.service';
import {ErrorPopupComponent} from "../../../shared-components/error-popup/error-popup.component";
@Component({
  selector: 'app-agent-groups-add',
  templateUrl: './agent-groups-add.component.html',
  styleUrls: ['./agent-groups-add.component.scss']
})
export class AgentGroupsAddComponent implements OnInit {

  public form: FormGroup;
  public allPermissions: Array<any>;

  @ViewChild(ErrorPopupComponent) errorPopup: ErrorPopupComponent;

  private Modell = {
    permissions: [],
    name: null,
    basic: null
  }

  constructor(private service: AgentgroupsService,
              private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    // this.allPermissions = this.route.snapshot.data['permissions'];
    this.form = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(5)]],
      basic: [false, []],
      permissions: [[], []]
    });

    this.service.getAllPermission().subscribe(
      res => this.allPermissions = res
    )
  }

  savePerrmissions($event, item){
    let itemUrl = item._links.self.href;

    if($event.target.checked){
      if(this.Modell.permissions.indexOf(itemUrl) == -1){
        this.Modell.permissions.push(itemUrl);
      }
    }

    if(!$event.target.checked){
      let index = this.Modell.permissions.indexOf(itemUrl);
      if(index !== -1){
        this.Modell.permissions.splice(index, 1)
      }
    }
  }

  save() {
    if(this.form.invalid) return;

    this.Modell.name = this.form.value.name;
    this.Modell.basic = this.form.value.basic;

    this.service.saveGroup(this.Modell)
      .subscribe(
        res => {
          this.router.navigate(['/agentgroups']);
        },

        err => this.errorPopup.showErrorPopup()
      );


  }

}
