import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AgentgroupsService, AgentGroup } from '../services/agentgroups.service';
import { Utils } from '../../../shared/utils';
import * as _ from "lodash";
import {ErrorPopupComponent} from "../../../shared-components/error-popup/error-popup.component";

@Component({
  selector: 'app-agent-groups-edit',
  templateUrl: './agent-groups-edit.component.html',
  styleUrls: ['./agent-groups-edit.component.scss'],
  // providers: [AgentgroupsService]
})
export class AgentGroupsEditComponent implements OnInit {

  @ViewChild(ErrorPopupComponent) errorPopup: ErrorPopupComponent;

  public allPermissions: Array<any>;
  public form: FormGroup;
  private original: AgentGroup;

  private Model = {
    name: null,
    permissions: []
  };

  constructor(private service: AgentgroupsService,
              private formBuilder: FormBuilder,
              private router:Router,
              private route: ActivatedRoute) {
  }

  private onChange(aValues) {
    this.Model.name = aValues.name;
  }

  ngOnInit() {
    this.original = this.route.snapshot.data['group'];
    this.allPermissions = this.route.snapshot.data['permissions'];

    this.form = this.formBuilder.group({
      name: [this.original.name, [Validators.required, Validators.minLength(5)]],
      basic: [this.original.basic, []],
      permissions: [this.original.permissions, []]
    });

    this.form.valueChanges.subscribe(value => this.onChange(value));

    this.Model.name = this.original.name;
    this.Model['id'] = this.original.id;
  }

  onChangePermissionList(data){
    this.Model.permissions = data;
  }

  save(){
    if(this.form.invalid) return;
    this.service.saveAgentGroup(this.Model)
      .subscribe(
        res => this.router.navigate(['/agentgroups']),
        err => this.errorPopup.showErrorPopup()
      );

  }
}
