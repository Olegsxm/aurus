import {Component, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SupervisorsService } from '../services/supervisors.service';
import { PasswordValidators } from 'ng2-validators';
import {ErrorPopupComponent} from "../../../shared-components/error-popup/error-popup.component";

@Component({
  selector: 'app-supervisors-edit',
  templateUrl: './supervisors-edit.component.html',
  styleUrls: ['./supervisors-edit.component.scss']
})
export class SupervisorsEditComponent implements OnInit {

  @ViewChild(ErrorPopupComponent) errorPopup: ErrorPopupComponent;

  public form: FormGroup;
  private original;

  constructor(private service: SupervisorsService,
              private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.original = this.route.snapshot.data['subervisor'];

    this.form = this.formBuilder.group({
      username:[this.original.username, [Validators.minLength(5)]],
      password: ['', []],
      confirmPassword: ['', []],
    }, PasswordValidators.mismatchedPasswords('password', 'confirmPassword'));
  }

  save() {
    if(this.form.invalid) return;

    this.service.edit(this.original.id ,this.form.value)
      .subscribe(
        res => {
          this.router.navigate(['/supervisors']);
        },
        err => this.errorPopup.showErrorPopup()
      );
  }

}
