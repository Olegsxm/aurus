import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SupervisorsService } from '../services/supervisors.service';
@Component({
  selector: 'app-supervisors-list',
  templateUrl: './supervisors-list.component.html',
  styleUrls: ['./supervisors-list.component.scss']
})
export class SupervisorsListComponent implements OnInit {
  public page: any;
  public items: any[];

  constructor(private service: SupervisorsService,
              private route: ActivatedRoute,
              private router: Router) {
    this.items = [];
  }

  ngOnInit() {
    this.load();
    let nextPage = +this.route.snapshot.params['page'] || 1;
    this.setPage(nextPage);

    this.service.getSupervisors().subscribe(
      res => this.items = res.filter( i => i.role == "SUPERVISOR")
    )
  }

  load(){
    this.service.page.subscribe(data => {
      // console.warn(data.list)
      // this.items = data.list.filter(i => i.role == 'SUPERVISOR');
      this.page = data.page;
    });
  }

  add() {
    this.router.navigate(['add'], {relativeTo: this.route});
  }

  edit(item: any) {
    let id = item._links.self.href.split('/').pop();
    this.router.navigate(['edit', id],
      { relativeTo: this.route });
  }

  remove(item: any) {
    this.deletedItem = item;
    this.displayConfirmWindow = true;

  }

  private displayConfirmWindow: boolean = false;
  private confirmWindowText: string = 'supervisor';
  private deletedItem = null;

  deleteItem($event){
    if($event){
      this.displayConfirmWindow = false;
      this.service.remove(this.deletedItem._links.self.href).subscribe(res => {
        this.items.splice(this.items.indexOf(this.deletedItem), 1);
        this.deletedItem = null;
      });
    }

    if(!$event){
      this.deletedItem = null;
      this.displayConfirmWindow = false;
    }
  }

  setPage(aPage: number) {
    this.service.load(aPage);
  };
}
