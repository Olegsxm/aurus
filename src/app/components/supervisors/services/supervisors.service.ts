import { Injectable } from '@angular/core';
import { JsonHttp } from '../../../shared/json-http';
import { URLSearchParams } from '@angular/http';
import { Subject, Observable } from 'rxjs';
import { AuthService } from '../../../shared/auth.service';
import * as _ from "lodash";

export interface Supervisor {
  id: string;
  name: string;
  username: string;
  password?: string;
  company?: string;
  role?:any;
}

@Injectable()
export class SupervisorsService {
  private apiUrl = 'adminUsers';
  private _page: any;
  private pageData: Subject<any>;
  private observable: Observable<any>;

  constructor(private http: JsonHttp,
    private authService: AuthService) {
    this.pageData = <Subject<any>>new Subject();
    this.observable = this.pageData.asObservable();
  }

  private mapSingleItem(aData: any): Supervisor {
    return <Supervisor>({
      id: aData._links.self.href,
      name: aData.name,
      username: aData.username,
    });
  }


  private mapListResponse(aResponse, aPage: number) {
    let data = aResponse.json();
    let result = {
      list: [],
      page: {
        totalItems: data.page.totalElements,
        itemsPerPage: data.page.size,
        currentPage: aPage
      }
    };
    let lst = typeof data._embedded == 'undefined' ? [] : data._embedded.adminUsers;
    for (let i = 0; i < lst.length; i++) {
      result.list.push(this.mapSingleItem(lst[i]));
    }
    return result;
  }

  private hadleSingleItem(aObservable) {
    return aObservable.map(response => {
      let data = response.json();
      return this.mapSingleItem(data);
    })
    .share();
  }

  load(aPage: number) {
    let params = new URLSearchParams();
    return this.http.get(this.apiUrl, params)
      .map(response => this.mapListResponse(response, aPage))
      .share()
      .subscribe(page => {
        this._page = page;
        this.pageData.next(this._page);
      });
  }

  create(aItem) {
    let data = _.cloneDeep(aItem);
    data.company = this.authService.companyUrl;
    return this.http.post(this.apiUrl, JSON.stringify(data))
      .share();
  }

  urlFromId(aId: string) {
    return this.apiUrl + '/' + aId;
  }

  idFromUrl(aUrl: string) {
    let parts = aUrl.split('/');
    return parts[parts.length - 1];
  }

  save(aItem: Supervisor) {
    let saveObject = _.cloneDeep(aItem);
    let url = saveObject.id;
    delete saveObject.id;
    return this.hadleSingleItem(this.http.patch(url, JSON.stringify(saveObject)));
  }

  get(aGroupUrl: string) {
    return this.hadleSingleItem(this.http.get(aGroupUrl));
  }

  remove(aGroupUrl: string) {
    return this.http.delete(aGroupUrl);
  }

  get page(): Observable<any> {
    return this.observable;
  }


  getCompanyUrl(){
    return this.http.get(this.authService.companyUrl)
      .map( res => res.json())
      .map(res => res._links.self.href)
  }


  getSupervisors(){
    return this.http.get('adminUsers').map(res => res.json())
      .map(res => {
        if(typeof res._embedded == "undefined"){
          return []
        }
        return res._embedded.adminUsers
      })
  }

  edit(url, data){
    console.warn(data.username)
    let Model = {
      username: data.username,
      role: "SUPERVISOR",
      company: this.authService.companyUrl
    };

    if(data.password.length > 0){
      Model['password'] = data.password;
    }

    return this.http.patch(url, JSON.stringify(Model))
  }
}
