import { Injectable }             from '@angular/core';
import { Router, Resolve,
         ActivatedRouteSnapshot } from '@angular/router';

import { Observable } from 'rxjs';
import { SupervisorsService, Supervisor } from './supervisors.service';

@Injectable()
export class SupervisorsItemResolver implements Resolve<Supervisor> {
  constructor(private service: SupervisorsService,
              private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<any>{
    return Observable.create(subject => {
      this.service.get(this.service.urlFromId(route.params['id'])).subscribe(data => {
        subject.next(data);
        subject.complete();
      });
    });
  }
}
