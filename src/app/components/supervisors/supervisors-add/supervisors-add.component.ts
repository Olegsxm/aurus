import {Component, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {SupervisorsService, Supervisor} from '../services/supervisors.service';
import { AbstractControl } from '@angular/forms';
import * as _ from "lodash";
import {AuthService} from "../../../shared/auth.service";
import {ErrorPopupComponent} from "../../../shared-components/error-popup/error-popup.component";

export class CustomValidators {

  static mismatchedPasswords(otherFiledName: string, showError: boolean) {

    function validate(aCtrl: AbstractControl): { [key: string]: boolean; } {
      let otherCtrl = aCtrl.root.get(otherFiledName);
      let result = null;
      let v1 = aCtrl.value;

      if (otherCtrl && otherCtrl.value !== v1) {
        if (showError) {
          result = { passwordsMismatched: false };
        } else {
          otherCtrl.setErrors({ passwordsMismatched: false });
        }
      } else {
        if (otherCtrl && !showError) {
          delete otherCtrl.errors['passwordsMismatched'];
          if (!Object.keys(otherCtrl.errors).length) {
            otherCtrl.setErrors(null);
          }
        }
      }
      //console.log('validate: ', result);
      return result;
    }

    return validate;
  }
}
@Component({
  selector: 'app-supervisors-add',
  templateUrl: './supervisors-add.component.html',
  styleUrls: ['./supervisors-add.component.scss']
})
export class SupervisorsAddComponent implements OnInit {

  @ViewChild(ErrorPopupComponent) errorPopup: ErrorPopupComponent;

  public form: FormGroup;

  constructor(private service: SupervisorsService,
              private formBuilder: FormBuilder,
              private router: Router,
              private authService: AuthService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(5)]],
      password: ['', [Validators.required,
        CustomValidators.mismatchedPasswords('confirmPassword', false)]],
      confirmPassword: ['', [Validators.required,
        CustomValidators.mismatchedPasswords('password', true)]],
    });

    this.form.valueChanges.subscribe(values => {
      console.log(this.form);
    });
  }

  save() {
    let data = {
      username: this.form['_value']['username'],
      password: this.form['_value']['password'],
     role: "SUPERVISOR",
    };

    // this.service.getCompanyUrl().subscribe(
    //   res => {
    //     data = {
    //       username: this.form['_value']['username'],
    //       password: this.form['_value']['password'],
    //       deleted: false,
    //       role: "SUPERVISOR",
    //       company: res
    //     };
    //
    //     this.service.create(data)
    //       .subscribe(result => {
    //           this.router.navigate(['/supervisors']);
    //         },
    //         error => {
    //           console.error('Failed to add supervisor: ', error);
    //         }
    //       );
    //   }
    // )

    this.service.getCompanyUrl()
      .mergeMap(res => {
        data['company'] = res;
        return this.service.create(data)
      })
      .subscribe(
        res => this.router.navigate(['/supervisors']),
        err => this.errorPopup.showErrorPopup()
      );
  }


}
