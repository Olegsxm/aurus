import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService }     from '../../shared';

@Component({
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public submitted: boolean;
  public errorMessage: string;

  constructor(private authService: AuthService,
              private router: Router,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.submitted = false;
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(1)]],
      password: ['', Validators.required]
    });
    this.errorMessage = '';
  }

  isDisabled() {
    return this.loginForm.invalid || this.submitted;
  }

  clearError() {
    this.errorMessage = '';
  }

  handleError(aError: any) {
    this.loginForm.setValue({
      username: '',
      password: ''
    });
    this.loginForm.markAsPristine();
    this.loginForm.markAsUntouched();
    this.errorMessage = aError;
    this.submitted = false;
  }

  login() {
    this.submitted = true;
    this.clearError();
    let val = this.loginForm.value;
    this.authService.login(val.username, val.password)
      .subscribe(result => {

      }, error => {
        this.handleError(error);
      });
  }
}
