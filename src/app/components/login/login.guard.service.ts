import { Injectable }       from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
}                           from '@angular/router';
import { AuthService } from '../../shared/auth.service';

@Injectable()
export class LoginGuard implements CanActivate {
  constructor(private authService: AuthService,
              private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canLogin();
  }

  canLogin(): boolean {
    if (this.authService.loggedIn) {
      this.router.navigate(['/']);
      return false;
    }
    return true;
  }
}
