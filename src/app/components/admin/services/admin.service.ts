import {Injectable} from "@angular/core";
import {JsonHttp} from "../../../shared/json-http";

@Injectable()

export class AdminService{

  constructor(private http: JsonHttp){}

  save(data){
    return this.http.post('/admin/password', JSON.stringify(data))
      .map(res => res.json())
  }
}
