import {Component, OnInit, ViewChild} from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder} from '@angular/forms';
import {AdminService} from "../services/admin.service";
import {Router} from "@angular/router";
import {ErrorPopupComponent} from "../../../shared-components/error-popup/error-popup.component";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
  providers: [AdminService]
})
export class ChangePasswordComponent implements OnInit {

  form: FormGroup;

  saveActive: boolean = false;

  @ViewChild(ErrorPopupComponent) errorPopup: ErrorPopupComponent;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private service: AdminService
  ) {

  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      oldPassword: ['', [Validators.required]],
      newPassword:['', [Validators.required]],
      confirmPassword: ['', [Validators.required]]
    });

    this.form.valueChanges.subscribe(val => {
      this.saveActive = !this.form.invalid;

      if(this.form.value['newPassword'] !== this.form.value['confirmPassword']){
        this.saveActive = false;
      }
    })
  }


  save(){
    if(this.form.invalid) return;

    let data = this.form.value;
    delete data.confirmPassword;
    this.service.save(data).subscribe(
      res => {
        this.router.navigate(['']);
      },
      err => this.errorPopup.showErrorPopup()
    )
  }


  confirmPass(pass, conf){

    if(conf == '') return false;

    if(pass !== conf && conf !== '') return true;

    return false;
  }
}
