import { Injectable }             from '@angular/core';
import { Router, Resolve,
         ActivatedRouteSnapshot } from '@angular/router';

import { Observable } from 'rxjs';
import { AgentsService, Agent } from './agents.service';

@Injectable()
export class AgentsItemResolver implements Resolve<Agent> {
  constructor(private service: AgentsService,
              private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {

    let id = route.params['id'];
    return Observable.create(subject => {
      this.service.get(this.service.urlFromId(id)).subscribe(data => {
        subject.next(data);
        subject.complete();
      });
    });
  }
}
