import { Injectable } from '@angular/core';
import {JsonHttp} from "../../../shared/json-http";
import {Headers, RequestOptions} from '@angular/http'
@Injectable()
export class GroupsService {

  constructor(
    private http: JsonHttp
  ) { }


  getAgentGroups(url){
    return this.http.get(url)
      .map(res => res.json())
      .map(res => res._embedded.agentGroups)
  }


  getAllGroups(){
    return this.http.get('agentGroups')
      .map(res => res.json())
      .map(res => res._embedded.agentGroups)
  }

  save(data, url){
    let options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'text/uri-list'
      })
    });

    let groups = '';

    data.map(i => groups += i + '\n' )

    return this.http.put(url , groups, options).map(res => res.json())
  }
}
