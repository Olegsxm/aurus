import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { URLSearchParams, RequestOptions, Headers } from '@angular/http';
import { JsonHttp } from '../../../shared/json-http';
import { Subject } from 'rxjs';
import { AuthService } from '../../../shared/auth.service';
import { AgentGroup } from '../../agent-groups/services/agentgroups.service';
import * as _ from "lodash";
import {isUndefined} from "util";

export interface Agent {
  id: string;
  fullName: string;
  lineNumber: string;
  username: string;
  password: string;
  active: boolean;
  company?: string;
  groups: AgentGroup[];
}

@Injectable()
export class AgentsService {
  private apiUrl = 'agents';
  private _page: any;
  private pageData: Subject<any>;
  private observable: Observable<any>;

  constructor(private http: JsonHttp,
    private authService: AuthService) {
    this.pageData = <Subject<any>>new Subject();
    this.observable = this.pageData.asObservable();


    this.http.get(this.apiUrl)
    .map(res => res.json())
    .subscribe(data => {console.log(data)}, err => console.log(err))
  }

  private mapSingleItem(aData: any): Agent {
    return <Agent>({
      id: aData._links.self.href,
      fullName: aData.fullName,
      lineNumber: aData.lineNumber,
      username: aData.username,
      active: aData.active,
      password: '',
      company: aData.company,
      groups: []
    });
  }

  load(aPage: number) {
    let params = new URLSearchParams();
    this.http.get(this.apiUrl, params)
      .map(response => {
        let data = response.json();
        let result = {
          list: [],
          page: {
            totalItems: data.page.totalElements,
            itemsPerPage: data.page.size,
            currentPage: aPage
          }
        };
        let that = this;
        let agents = typeof data._embedded == "undefined" ? [] : data._embedded.agents
        _.forEach(agents, function (aAgent) {
          result.list.push(that.mapSingleItem(aAgent));
        });
        return result;
      })
      .share()
      .subscribe(page => {
        this._page = page;
        this.pageData.next(this._page);
      });
  }

  idFromUrl(aUrl: string): string {
    let parts = aUrl.split('/');
    return parts[parts.length - 1];
  }

  urlFromId(aId: string) {
    return this.apiUrl + '/' + aId;
  }

  create(aAgent: Agent) {
    let data = _.cloneDeep(aAgent);
    data.company = this.authService.companyUrl;
    return this.http.post(this.apiUrl, JSON.stringify(data));
  }

  get(aUrl: string) {
    return this.http.get(aUrl)
      .map(response => response.json());
  }

  remove(aUrl: string) {
    return this.http.delete(aUrl).subscribe(result => {
      this.load(this._page.cureentPage);
     });
  }

  save(aAgent) {
    console.warn(aAgent)
    let saveObject = _.cloneDeep(aAgent);
    let url = saveObject.id;
    delete saveObject.id;

    let observable = this.http.patch(url, JSON.stringify(saveObject));

    return observable.map(response => {
      let data = response.json();
      return this.mapSingleItem(data);
    }).share();
  }

  get page(): Observable<any> {
    return this.observable;
  }

  getAgentById(id){
    return this.http.get(this.apiUrl + id)
      .map(res => res.json())
      .map(res => res._embedded.agents)
      //.map(res => res.filter(i => i._links.self.href.split('/').pop() == id))
  }



  getAgentGroups(data){
    return this.http.get(data._links.groups.href)
      .map(res => res.json())
      .map(res => res._embedded.agentGroups)
  }

  getAllGroups(){
    return this.get('/admin/agentGroups').map(res => res._embedded.agentGroups)
  }

  saveChenges(data){
    let url = data._links.agentGroup.href;
    delete data._links;
    return this.http.patch(url, JSON.stringify(data)).map(res => res.json())
  }


  saveAgentPassword(agent, password){
    return this.http.patch(agent, JSON.stringify({password: password}))
      .map(res => res.json())
  }

  convertAvatar(file, cb){
    if(isUndefined(file)){
      cb(null);
      return;
    }
    file.convertToBase64(function (e) {
      cb(e);
    })
  }

  saveAvatar(data, avatar){
    let url = data._links.self.href;

    let img = avatar;

    return this.http.patch(url, JSON.stringify({
      avatarBase64: img
    })).map(res => res.json())
  }

  removeAvatar(url){
    return this.http.delete(url);
  }

  checkAvatar(url){
    return this.http.get(url)
      .map(res => {return res.status == 404 ? false: true})
  }
}
