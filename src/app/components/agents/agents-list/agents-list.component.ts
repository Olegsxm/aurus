import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AgentsService } from '../services/agents.service';

@Component({
  selector: 'app-agents-list',
  templateUrl: './agents-list.component.html',
  styleUrls: ['./agents-list.component.scss']
})
export class AgentsListComponent implements OnInit {
  public page: any;
  public items: any[];

  constructor(private service: AgentsService,
              private route: ActivatedRoute,
              private router: Router) {
    this.items = [];
  }

  ngOnInit() {
    this.service.page.subscribe(data => {
      this.items = data.list;
      this.page = data.page;
    });
    let nextPage = +this.route.snapshot.params['page'] || 1;
    this.setPage(nextPage);
  }

  add() {
    this.router.navigate(['add'], {relativeTo: this.route});
  }

  edit(item: any) {
    // debugger
    // let itemId: string = item.id.split('/').pop();

    this.router.navigate(['edit', this.service.idFromUrl(item.id)],
      { relativeTo: this.route });
  }

  remove(item: any) {
    // if(!window.confirm("Delete?")) return;
    // this.service.remove(item.id);
    this.displayConfirmWindow = true;
    this.deletedItem = item;
  }


  setPage(aPage: number) {
    this.service.load(aPage);
  };

  private displayConfirmWindow: boolean = false;
  private confirmWindowText: string = 'agent';
  private deletedItem = null;

  deleteItem($event){
    if($event){
      this.service.remove(this.deletedItem.id);
      this.items.splice(this.items.indexOf(this.deletedItem), 1);
      this.displayConfirmWindow = false;
    }

    if(!$event){
      this.deletedItem = null;
      this.displayConfirmWindow = false;
    }
  }
}
