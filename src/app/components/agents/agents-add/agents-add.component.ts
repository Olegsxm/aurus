import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AgentsService } from '../services/agents.service';
import { AgentGroup } from '../../agent-groups/services/agentgroups.service';
import * as _ from "lodash";
import {GroupsService} from "../services/groups.service";
import {isNull} from "util";
import {ErrorPopupComponent} from "../../../shared-components/error-popup/error-popup.component";

@Component({
  selector: 'app-agents-add',
  templateUrl: './agents-add.component.html',
  styleUrls: ['./agents-add.component.scss'],
  providers: [GroupsService]
})
export class AgentsAddComponent implements OnInit {

  @ViewChild(ErrorPopupComponent) errorPopup: ErrorPopupComponent;

  public allGroups: AgentGroup[];
  public form: FormGroup;
  private avatar = null;
  private delAvatar: boolean = false;

  private Model = {
    groups: []
  };

  constructor(private service: AgentsService,
              private formBuilder: FormBuilder,
              private router: Router,
              private groupService: GroupsService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    let selected = [];
    let _this = this;

    this.allGroups = this.route.snapshot.data['groups'];
    _.forEach(this.allGroups, function (aGroup) {
      if (aGroup.basic) {
        if(_this.Model.groups.indexOf(aGroup.id) == -1){
          _this.Model.groups.push(aGroup.id);
        }
        selected = [aGroup.id];
        return false;
      }
    });

    this.allGroups = this.allGroups.filter(i => i.basic == false);
    this.form = this.formBuilder.group({
      fullName: ['', [Validators.required, Validators.minLength(1)]],
      lineNumber: ['', []],
      username: ['', [Validators.required, Validators.minLength(5)]],
      password: ['', [Validators.required, Validators.minLength(5)]],
      confirm: ['', [Validators.required, Validators.minLength(5)]]
    });
  }

  save() {

    let data = this.form.value;

    data["avatarBase64"] = this.avatar;

    this.service.create(data)
      .map(res => res.json())
      .subscribe(
        result => {
          this.groupService.save(this.Model.groups, result['_links'].groups.href).subscribe(
              res => {
                  this.router.navigate(['/agents']);
              },

            err => this.errorPopup.showErrorPopup()
          );

          // if(this.avatar !== null){
          //   if(this.delAvatar){
          //     this.avatar = '';
          //   }
          //
          //   this.service.saveAvatar(result, this.avatar).subscribe(
          //     res => this.router.navigate(['/agents']),
          //     err => this.errorPopup.showErrorPopup()
          //   );
          // }
        },
        error => {
          console.error('Failed to add agent: ', error);
          err => this.errorPopup.showErrorPopup();
        }
      );
  }

  saveGroups($event, item){
    let id = item.id;

    if($event.target.checked){
      if(this.Model.groups.indexOf(id) == -1){
        this.Model.groups.push(id)
      }
    }

    if(!$event.target.checked){
      let index = this.Model.groups.indexOf(id);
      this.Model.groups.splice(index, 1);
    }
  }


  savePhoto($event){
    let file = $event.target.files[0];

    if(file == undefined){
      this.avatar = null;
      return;
    }

    this.service.convertAvatar(file, data => {
      this.avatar = data.split(',')[1];
    });
  }
}
