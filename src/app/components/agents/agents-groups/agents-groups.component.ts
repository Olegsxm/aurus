import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import {GroupsService} from "../services/groups.service";

@Component({
  selector: 'app-agents-groups',
  templateUrl: './agents-groups.component.html',
  styleUrls: ['./agents-groups.component.scss'],
  providers: [GroupsService]
})
export class AgentsGroupsComponent implements OnInit {

  constructor(
    private service: GroupsService
  ) { }

  @Input('data') agentGroups = null;
  @Output() changeGroup = new EventEmitter();

  agentGroupList = [];

  allGroups = [];
  groupModel = [];

  private agentUrl: string;


  ngOnChanges(){
    this.agentUrl = this.agentGroups['_links']['groups']['href'];
  }

  ngOnInit() {

    this.service.getAgentGroups(this.agentGroups['_links']['groups']['href'])
      .subscribe(
        res => this.agentGroupList = res,
        err => console.error(err),
        () => {
          this.setModel();
        }
      )

    this.service.getAllGroups().subscribe(
      res => this.allGroups = res.filter(i => i.basic !== true),
      err => console.error(err),
      () => {}
    );
  }

  setModel(){
    this.agentGroupList.map( i => {
      let agentUrl = i._links.self.href;
      if(this.groupModel.indexOf(agentUrl) == -1){
        this.groupModel.push(agentUrl)
      }
    });
    this.changeGroup.emit(this.groupModel);
  }

  compareGroups(item){
    let itemUrl = item._links.self.href;
    let result = false;

    this.agentGroupList.map(i => {
      if(i._links.self.href == itemUrl){
        result = true;
      }
    });

    return result;
  }

  saveGroups($event, item){
    let itemUrl = item._links.self.href;

    if($event.target.checked){
      if(this.groupModel.indexOf(itemUrl) == -1){
        this.groupModel.push(itemUrl);
      }
    }

    if(!$event.target.checked){
      let index = this.groupModel.indexOf(itemUrl)

      if(index !== -1){
        this.groupModel.splice(index, 1)
      }
    }

    this.changeGroup.emit(this.groupModel);
    // this.service.save(this.groupModel, this.agentUrl).subscribe(
    //   res => console.warn(res)
    // )
  }
}
