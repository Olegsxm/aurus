import { Component, OnInit, ViewChild } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { AgentsService, Agent } from '../services/agents.service';
import { AgentGroup } from '../../agent-groups/services/agentgroups.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Utils } from '../../../shared/utils';
import * as _ from "lodash";
import {GroupsService} from '../services/groups.service';
import { ErrorPopupComponent } from "../../../shared-components/error-popup/error-popup.component";
import {Observable} from "rxjs";

@Component({
  selector: 'app-agents-edit',
  templateUrl: './agents-edit.component.html',
  styleUrls: ['./agents-edit.component.scss'],
  providers: [GroupsService]
})
export class AgentsEditComponent implements OnInit {
  constructor(private service: AgentsService,
              private formBuilder: FormBuilder,
              private groupService: GroupsService,
              private router:Router,
              private route: ActivatedRoute) {
  }

  @ViewChild(ErrorPopupComponent) errorPopup: ErrorPopupComponent;

  private agent;

  private original: Agent;
  public allGroups: AgentGroup[];
  public form: FormGroup;

  private avatar = null;
  private delAvatar: boolean = false;
  private hasAvatar = false;

  private password: string = '';
  private confirmPassword: string = '';

  private Model = {
    data: {},
    datalen: 0,
  };

  changePassword: boolean = false;
  validPassword: boolean = false;

  private onChange(aValues) {
    let newData = <Agent>Utils.getOnlyChanged(this.original, aValues);
    let counter = 0;

    for(let i in newData){
      if(newData[i] !== undefined){
        this.Model.data[i] = newData[i];
        counter++;
      }
    }
    this.Model['datalen'] = counter;
  }

  ngOnInit() {
    this.original = this.route.snapshot.data['agent'];
    let selected = [];

    this.service.checkAvatar(this.original['_links'].avatar.href)
      .subscribe(
        res => this.hasAvatar = res,
        err => console.clear()
      );

    this.form = this.formBuilder.group({
      fullName: [this.original.fullName, [Validators.required, Validators.minLength(1)]],
      lineNumber: [this.original.lineNumber, []],
      username: [this.original.username, [Validators.required, Validators.minLength(5)]],
      password: ['', [Validators.required, Validators.minLength(5)]],
      groups: [selected, []]
    });

    this.form.valueChanges.subscribe(value => this.onChange(value));
  }

  validatePassword($event){
    let result = false;

    if(this.confirmPassword !== this.password){
      $event.classList.add('error');
      this.validPassword = false;
    } else if(this.confirmPassword.length == 0 && this.password.length == 0){
      this.validPassword = false;
    } else {
      result = true;
      $event.classList.remove('error');
      this.validPassword = true;
    }

    return result;
  }

  validatePasswordLength($event){
    let result = false;
    if(this.password.length < 5){
      $event.classList.add('error');
      this.validPassword = false;
    } else {
      if(this.password == this.confirmPassword || this.confirmPassword.length == 0){
        result = true;
        $event.classList.remove('error');
      }
    }
    return result;
  }

  onChangeGroup($event){
    this.Model['groups'] = $event;
  }


  savePassword(){
    if(this.confirmPassword == this.password
      && this.confirmPassword.length >= 5
      && this.password.length >= 5
    ){
      this.service.saveAgentPassword(this.original['_links'].self.href, this.password)
        .subscribe(
          res => console.warn(res)
        )
    } else {
      alert('Пароль не')
    }
  }

  save(){

    let requestArr = [];

    let $data  = null;
    let $avatar = null;

    if(this.avatar !== null || this.delAvatar){
      if(this.delAvatar){
        this.Model.data['avatarBase64'] = "";
        $avatar = this.service.removeAvatar(this.original['_links'].avatar.href);
        requestArr.push(this.service.removeAvatar(this.original['_links'].avatar.href));
        // this.service.removeAvatar(this.original['_links'].avatar.href)
        //   .subscribe(
        //     res =>  this.router.navigate(['/agents'])
        //   );
      }
      else {
        //$avatar = this.service.saveAvatar(this.original, this.avatar);
        this.Model.data['avatarBase64'] = this.avatar;
        requestArr.push(this.service.saveAvatar(this.original, this.avatar));
        // this.service.saveAvatar(this.original, this.avatar).subscribe(
        //   res => {
        //     this.router.navigate(['/agents']);
        //   }
        // );
      }
    }

    if(this.changePassword && this.validPassword){
      this.Model.data['password'] = this.password;
    }

    // if(this.Model.datalen > 0){
      this.Model.data['id'] = this.original['_links'].self.href;
      $data = this.service.save(this.Model.data);

      requestArr.push(this.service.save(this.Model.data));
    // }

    let $groups = this.groupService.save(this.Model['groups'],this.original['_links'].groups.href);

    $groups.subscribe(
      res => {
        if($data !== null){
          $data.subscribe(
            res => this.router.navigate(['/agents']),
            err => this.errorPopup.showErrorPopup()
          );
        }
        if($data == null){
          this.router.navigate(['/agents'])
        }
      },
      err => this.errorPopup.showErrorPopup()
    );
  }

  savePhoto($event){
    let file = $event.target.files[0];

    this.service.convertAvatar(file, data =>{
      this.avatar = data.split(',')[1];
    });
  }
}
