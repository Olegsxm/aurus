import { Component, OnInit, Input, OnChanges, EventEmitter, Output} from '@angular/core';
import {PermissionService} from "../services/permission.service";


@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss'],
  providers: [PermissionService]

})
export class PermissionsComponent implements OnInit, OnChanges {
  @Input('data') companyPeemissionsList = [];
  @Output() changePermissionModel = new EventEmitter();

  private permissionsList = [];
  private permissionsModelList : string[] = [];

  constructor(
    private service: PermissionService,
  ) {

  }

  ngOnInit() {
    this.service.getPermissionsList().subscribe(
      res => this.permissionsList = res,
      err => console.error(err),
      () => {
        //this.setModel()
      }
    )
  }


  comparePerrmissions(item){
    let result = false;
    for (let i of this.companyPeemissionsList){
      if (i.type === item.type){
        result = true;

        continue;
      }
    }
    return result;

  }


  savePerrmissions($event, item){
    let itemUrl = item._links.self.href;

    if($event.target.checked){
      if(this.permissionsModelList.indexOf(itemUrl) == -1){
        this.permissionsModelList.push(itemUrl);
      }
    }

    if(!$event.target.checked){
      let index = this.permissionsModelList.indexOf(itemUrl);
      if(index !== -1){
        this.permissionsModelList.splice(index, 1)
      }
    }

    //this.service.savePermission(item, this.permissionsModelList)
    this.changePermissionModel.emit(this.permissionsModelList)
  }


  setModel(){
    this.companyPeemissionsList.map(i => {
      let url = i._links.self.href;
      if(this.permissionsModelList.indexOf(url) == -1){
        this.permissionsModelList.push(url)
      }
    })

    this.changePermissionModel.emit(this.permissionsModelList)
  }

  ngOnChanges(){
    this.setModel()
  }
}
