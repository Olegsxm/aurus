import { Component, OnInit,  ViewChild} from '@angular/core';
import {SettingsService} from "./services/settings.service";
import {FormGroup, FormBuilder, Validators, Form} from "@angular/forms";
import {SaveService} from "./services/save.service";
import {Router} from "@angular/router";
import {ErrorPopupComponent} from "../../shared-components/error-popup/error-popup.component";

@Component({
  templateUrl: './settings.component.html',
  providers: [SettingsService, SaveService]
})

export class SettingsComponent implements OnInit {

  @ViewChild(ErrorPopupComponent) errorPopup: ErrorPopupComponent;

  private Company: ICompany = {
    companyName: '',
    permissions: [],
    settings: [],
    companyUrl: ''
  };

  private DataModel = {
    permissions: null,
    settings: [],
    recordAgetnsVideo:{},
    recordClientsVideo:{}
  };

  quickMenu = {
    prefix: 'settings',
    links: [
      {
        id: 'companyName',
        title: 'Название компании'
      },

      {
        id: 'sessionSettings',
        title: 'Настройки сессии'
      },

      {
        id: 'videoRecordSettings',
        title: 'Настройки записи видео'
      },

      {
        id: 'perrmissions',
        title: 'Функции, доступные для клиентов'
      }
    ],
    getHref: function(item){return this.prefix + '#' + item.id}
  };

  settingsModel = {
    deleteSessionTimeout: {},
    webcamFeedTimeout: {},
    desktopFeedTimeout: {},
    maxSessionLength: {},
    recordAgetnsVideo: {},
    recordClientsVideo: {},
  };

  constructor(
    private  service: SettingsService,
    private formBuilder: FormBuilder,
    private saveService: SaveService,
    private router:Router
  ){}

  ngOnInit(){
    this.service.getCompanyInfo().subscribe(
      res => {
        this.setCompanyName(res.name);
        this.setPermissions(res._links.permissions.href);
        this.setCompanySettings(res._links.settings.href);
        this.Company.companyUrl = res._links.self.href;
      },
      err => console.error(err),

      () => {

      }
    )


  }



  setSettingsModel(arr){
    let _this = this;

    for (let i of arr){
      swit(i)
    }

    this.DataModel['recordAgetnsVideo'] = this.settingsModel.recordAgetnsVideo;
    this.DataModel['recordClientsVideo'] = this.settingsModel.recordClientsVideo;
    function swit(i){
      if(i.key == 'DELETE_SESSION_TIMEOUT'){ _this.settingsModel.deleteSessionTimeout = i }

      if(i.key == 'WEBCAM_FEED_TIMEOUT'){ _this.settingsModel.webcamFeedTimeout = i}

      if(i.key == 'DESKTOP_FEED_TIMEOUT'){ _this.settingsModel.desktopFeedTimeout = i }

      if(i.key == 'MAXIMUM_SESSION_LENGTH'){ _this.settingsModel.maxSessionLength = i }

      if(i.key == 'RECORD_AGENT_WEB_CAMERA_FEED'){ _this.settingsModel.recordAgetnsVideo = i }

      if(i.key == 'RECORD_CLIENT_WEB_CAMERA_FEED'){_this.settingsModel.recordClientsVideo = i}

    }
  }


  setCompanyName(name){
    this.Company.companyName = name;
  }
  setPermissions(link){
    this.service.getPerrmissions(link).subscribe(
      res => {
        this.Company.permissions = res.permissions
      }
    )
  }
  setCompanySettings(url){
    this.service.getCompanySettings(url).subscribe(
      res => {
        this.Company.settings = res.companySettings
        this.setSettingsModel(this.Company.settings);
        this.DataModel.settings = res.companySettings;
      },
      err => console.error(err)
    )
  }

  changeCompanyName(input: HTMLInputElement){

    if(input.value.length < 5){
      if(!input.classList.contains('error')){
        input.classList.add('error');
        return;
      }
    }

    if(input.value.length >= 5){
      if(input.classList.contains('error')){
        input.classList.remove('error')
      }

      this.DataModel['companyName'] = input.value;
      // this.service.setCompanyName(input.value, this.Company.permissions).subscribe(
      //   res => console.warn(res),
      //   err => alert('error. 403 status')
      // )
    }
  }

  //Валидация полей настроек
  validateSetting(type: string, input:HTMLInputElement){
    if(Validator.IsNumber(input.value) && Validator.Min(input.value)){
      this.settingsModel[type].value = input.value;
      let data = this.settingsModel[type];
      input.classList.remove('error');
      this.saveCompanySettings(this.settingsModel[type])
    } else {
      input.classList.add('error');
    }
  }

  //Записать настройки
  saveCompanySettings(data){
    let index = this.DataModel.settings.indexOf(data);
    this.DataModel.settings[index] = data;

    // this.service.saveCompanySetting(data).subscribe(
    //   res => console.warn(res),
    //   err => console.error(err)
    // )
  }

  //Чекбоксы
  setVideoRecords(type: string, checked: boolean){

    if(type == 'agent'){
      let data: ICompanySetting = {
        key: "RECORD_AGENT_WEB_CAMERA_FEED",
        value: checked.toString(),
        required: false,
        settingClass: "BOOLEAN",
        minValue: null,
        maxValue: null,
        regExp: null,
        company: this.Company.companyUrl,
      };
      this.DataModel.recordAgetnsVideo = data;
      // this.service.setVideoRecordsSettings(this.settingsModel.recordAgetnsVideo['_links']['self']['href'], data)
      //   .subscribe(
      //     res => console.warn(res)
      //   )
    }

    if(type == 'client'){
      let data: ICompanySetting = {
        key: "RECORD_CLIENT_WEB_CAMERA_FEED",
        value: checked.toString(),
        required: false,
        settingClass: "BOOLEAN",
        minValue: null,
        maxValue: null,
        regExp: null,
        company: this.Company.companyUrl,
      };
      this.DataModel.recordClientsVideo = data;
      // this.service.setVideoRecordsSettings(this.settingsModel.recordClientsVideo['_links']['self']['href'], data)
      //   .subscribe()
    }
  }

  isChecked(val){
    return val == 'true'? true: false;
  }

  //Клиентские разрешения
  savePerrmissions(e, item){
    console.warn(e)
  }

  onChangePermissionModel(list: Array<any>){
    this.DataModel['permissions'] = list;
  }


  save(){
    let errorCounter = 0;

    this.DataModel['companyName'] = this.Company.companyName;
    this.DataModel['companyUrl'] = this.Company.companyUrl;
    this.DataModel['recordAgetnsVideoUrl'] = this.settingsModel.recordAgetnsVideo['_links']['self']['href'];
    this.DataModel['recordClientsVideoUrl'] = this.settingsModel.recordClientsVideo['_links']['self']['href'];

    this.saveService.save(this.DataModel).subscribe(
      res => {},
      err => ++errorCounter,

      () => {
        console.warn(errorCounter)
        if(errorCounter == 0){
          this.router.navigate(['/admin']);
        } else {
          this.errorPopup.showErrorPopup();
        }
      }
    );
  }
}

export class Validator{

  static IsNumber(val){

    if(typeof val == "number" || typeof parseInt(val) == "number" && !isNaN(parseInt(val))){
      return true;
    }

    return false;
  }


  static Min(value, dValue = 0){
    if(this.IsNumber(value)){
      return parseInt(value) >= 0
    }
  }
}


export interface ICompany{
  companyName: string;
  permissions: Array<IPermission>;
  settings: Array<ICompanySetting>;
  companyUrl: string;
}

export interface IPermission{
  _links: Array<Object>,
  type: string,
  description: string,
  order:number,
  authority: string
}

export interface ICompanySetting{
  _links?: Array<Object>,
  key: string,
  value: string,
  required: boolean,
  settingClass: string,
  minValue: string,
  maxValue: string,
  regExp: string,
  company: string
}
