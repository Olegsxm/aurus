import {Injectable} from "@angular/core";
import {JsonHttp} from "../../../shared/json-http";
import {
  RequestOptions,
  Headers,
} from '@angular/http';
import {PermissionService} from "./permission.service";
import {SettingsService} from "./settings.service";
import {Observable} from "rxjs";

@Injectable()
export class SaveService{

  constructor(
    private http: JsonHttp,
    //private permissionService: PermissionService,
    private settingsServise: SettingsService
  ){
    this.settingsServise.getCompanyInfo().subscribe(
      res => this.companyInfo = res,
      err => console.warn(err),
      () => console.warn(this.companyInfo)
    );
  }

  private companyInfo;

  save(data){
    let requestArray = [this.settingsServise.setCompanyName(data.companyName)];

    for(let i of data.settings){
      requestArray.push(this.settingsServise.saveCompanySetting(i))
    }


    requestArray.push(this.settingsServise.setVideoRecordsSettings(data.recordAgetnsVideoUrl, data.recordAgetnsVideo));

    requestArray.push(this.settingsServise.setVideoRecordsSettings(data.recordClientsVideoUrl, data.recordClientsVideo));


    let options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'text/uri-list'
      })
    });

    let permissions = '';
    data.permissions.map(i => permissions += (i + '\n'));

    requestArray.push(this.http.put(this.companyInfo._links.permissions.href, permissions, options));

    return Observable.forkJoin(requestArray)
  }

  savePermission(companyPermission){

    let options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'text/uri-list'
      })
    });

    this.settingsServise.getCompanyInfo().subscribe(
      res => {
        let permissions = '';
        companyPermission.map(i => permissions += (i + '\n'));

        this.http.put(res._links.permissions.href, permissions, options).subscribe(
          res => {},
          err => console.error(err)
        )
      },

      err => console.error(err)
    )
  }
}
