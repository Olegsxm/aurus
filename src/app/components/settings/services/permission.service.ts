/**
 * Created by sxm on 06.02.2017.
 */

import {Injectable} from "@angular/core";
import {JsonHttp} from "../../../shared/json-http";
import {SettingsService} from "../services/settings.service";
import {
  RequestOptions,
  Headers,
} from '@angular/http';

@Injectable()
export class PermissionService{


  constructor(
    private http: JsonHttp,
    private settingsService: SettingsService
  ){

  }

  getPermissionsList(){
    return this.http.get('permissions').map(res => res.json())
      .map(res => res._embedded.permissions);
  }


  savePermission(item, companyPermission){

    let options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'text/uri-list'
      })
    });

    this.settingsService.getCompanyInfo().subscribe(
      res => {
        let permissions = '';
        companyPermission.map(i => permissions += (i + '\n'));

        this.http.put(res._links.permissions.href, permissions, options).subscribe(
          res => {},
          err => console.error(err)
        )
      },

      err => console.error(err)
    )
  }

}
