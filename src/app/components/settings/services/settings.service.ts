import { Injectable } from '@angular/core';
import { JsonHttp } from '../../../shared/json-http';
import {AuthService} from "../../../shared/auth.service";

export interface Settings {
  deleteSessionTimeout: number;
  maxSessionLength: number;
  desktopFeedTimeout: number;
  webcamFeedTimeout: number;
  recordAgetnsVideo: boolean;
  recordClientsVideo: boolean;
}

@Injectable()
export class SettingsService  {


  apiUrl: string;

  constructor(
    private http: JsonHttp,
    private authServis: AuthService
  ) {
    this.apiUrl = this.authServis.companyUrl;
  }



  getCompanyInfo(){
    return this.http.get(this.apiUrl).map(res => res.json())
  }

  getPerrmissions(url){
    return this.http.get(url).map(res => res.json())
      .map(res => res._embedded);
  }

  getCompanySettings(url){
    return this.http.get(url)
      .map(res => res.json())
      .map(res => res._embedded)
  }


  setCompanyName(value){
    let company = {
      name: value
    };


    //permission.map(i => company.permissions.push(i._links.self.href));

    return this.http.patch(this.apiUrl, JSON.stringify(company))
      .map(res => res.json())
  }

  setVideoRecordsSettings(url, data){
    return this.http.patch(url, JSON.stringify(data)).map(res => res.json())
  }

  saveCompanySetting(data){
    return this.http.patch(data._links.self.href, JSON.stringify(data))
      .map(res => res.json())
  }
}
