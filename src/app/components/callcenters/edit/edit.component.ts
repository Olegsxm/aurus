import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CallcenterService, Contactcenter } from  '../services/callcenters.service';
import { Utils } from '../../../shared/utils';
import * as _ from "lodash";
import {ErrorPopupComponent} from "../../../shared-components/error-popup/error-popup.component";

export interface ISEttings{
  _links: Object;
  key: string;
  maxValue: string;
  minValue: string;
  regExp: string;
  required: boolean;
  settingClass: string;
  value: string;
}
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  @ViewChild(ErrorPopupComponent) errorPopup: ErrorPopupComponent;

  public form: FormGroup;
  public settingsForm: FormGroup;
  private original;


  formChanged:boolean = false;

  quickMenu = {
    prefix: 'settings',
    links: [
      {
        id: 'companyName',
        title: 'Название компании'
      },

      {
        id: 'sessionSettings',
        title: 'Настройки сессии'
      },

      {
        id: 'videoRecordSettings',
        title: 'Настройки записи видео'
      },

      {
        id: 'perrmissions',
        title: 'Функции, доступные для клиентов'
      }
    ],
    getHref: function(item){return this.prefix + '#' + item.id}
  };


  private Model;

  private settings = {
    phoneNumberPrefix: {value: 10, searchKey: 2},
    phoneNumberPattern:{value: 10, searchKey: 2},
    browserCallPrefix: {value: 10, searchKey: 2},
    requestSessionTimeout: {value: 10, searchKey: 3},
    callCenterTimeout: {value: 10, searchKey: 2},
    clientCallTimeout: {value: 10, searchKey: 2},
    browserCallTimeout:{value: 10, searchKey: 2},
  };

  constructor(
    private service: CallcenterService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute)
  {}

  getSettings(){
    let url = typeof this.original !== "string" ? this.original._links.settings.href : JSON.parse(this.original)._links.settings.href;
    this.service.getCallCenterSettings(url).subscribe(res =>{
      let settings = res.callCenterSettings;

      this.settings.phoneNumberPrefix = settings.filter(i => i.key == "PHONE_NUMBER_PREFIX")[0];
      this.settings.phoneNumberPattern = settings.filter(i => i.key == "PHONE_NUMBER_PATTERN")[0];
      this.settings.browserCallPrefix = settings.filter(i => i.key == "BROWSER_CALL_PREFIX")[0];
      this.settings.requestSessionTimeout = settings.filter(i => i.key == "REQUEST_SESSION_TIMEOUT")[0];
      this.settings.callCenterTimeout = settings.filter(i => i.key == "CALL_CENTER_CALL_TIMEOUT")[0];
      this.settings.clientCallTimeout = settings.filter(i => i.key == "CLIENT_CALL_TIMEOUT")[0];
      this.settings.browserCallTimeout = settings.filter(i => i.key == "BROWSER_CALL_TIMEOUT")[0];

      this.buildForm()
    })
  }

  ngOnInit() {

    this.original = this.route.snapshot.data['callcenter']._body;

    if(typeof this.original == "string"){
      this.original = JSON.parse(this.route.snapshot.data['callcenter']._body);
    } else {
      this.original = this.route.snapshot.data['callcenter']._body;
    }

    this.getSettings();

    this.buildForm()

  }
  private onChange(type:string, aValues:string) {
    this.formChanged = true;
  }

  buildForm(){
    this.form = this.formBuilder.group({
      description: [this.original.description, [Validators.required, Validators.minLength(5)]],
      phoneNumber: [this.original.phoneNumber, [Validators.required]],
      ipAddress: [this.original.ipAddress, [Validators.required, Validators.pattern('^([0-9]{1,3})[.]([0-9]{1,3})[.]([0-9]{1,3})[.]([0-9]{1,3})$')]],
      phoneNumberMask: [this.original.phoneNumberMask, [Validators.required]],

      settings: this.formBuilder.group({
        phoneNumberPrefix: [this.settings.phoneNumberPrefix.value, [Validators.required]],
        phoneNumberPattern: [this.settings.phoneNumberPattern.value, [Validators.required]],
        browserCallPrefix: [this.settings.browserCallPrefix.value],
        requestSessionTimeout: [this.settings.requestSessionTimeout.value, [Validators.required]],
        callCenterTimeout: [this.settings.callCenterTimeout.value, [Validators.required]],
        clientCallTimeout: [this.settings.clientCallTimeout.value, [Validators.required]],
        browserCallTimeout: [this.settings.browserCallTimeout.value, [Validators.required]]
      })
    });


  }


  save(){
    if(!this.formChanged) return;

    let id = document.location.href.split('/').pop();

    this.Model = {
      basic:{
        description: this.form.controls['description']['_value'],
        phoneNumber: this.form.controls['phoneNumber']['_value'],
        ipAddress: this.form.controls['ipAddress']['_value'],
        phoneNumberMask: this.form.controls['phoneNumberMask']['_value'],
      },

      settings:{
        phoneNumberPrefix: this.form.controls['settings']['controls']['phoneNumberPrefix']['_value'],
        phoneNumberPattern: this.form.controls['settings']['controls']['phoneNumberPattern']['_value'],
        browserCallPrefix: this.form.controls['settings']['controls']['browserCallPrefix']['_value'],
        requestSessionTimeout: this.form.controls['settings']['controls']['requestSessionTimeout']['_value'],
        callCenterTimeout: this.form.controls['settings']['controls']['callCenterTimeout']['_value'],
        clientCallTimeout: this.form.controls['settings']['controls']['clientCallTimeout']['_value'],
        browserCallTimeout: this.form.controls['settings']['controls']['browserCallTimeout']['_value']
      }
    };

    this.service.save(id, this.Model,this.original._links.self.href, this.settings)
      .subscribe(
        res => this.router.navigate(['/contactcenters']),

        err => this.errorPopup.showErrorPopup()
      );

  }
}
