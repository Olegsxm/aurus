import { Component, OnInit } from '@angular/core';
import {CallcenterService} from "./services/callcenters.service";
import {ContactcentersItemResolver} from "./services/callcenter.resolve";

@Component({
  selector: 'app-callcenters',
  templateUrl: './callcenters.component.html',
  styleUrls: ['./callcenters.component.scss']
})
export class CallcentersComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
