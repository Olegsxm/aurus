import { Injectable }             from '@angular/core';
import { Router, Resolve,
         ActivatedRouteSnapshot } from '@angular/router';

import { Observable } from 'rxjs';
import { CallcenterService, Contactcenter } from './callcenters.service';
import { JsonHttp } from '../../../shared/json-http';

@Injectable()
export class ContactcentersItemResolver implements Resolve<Contactcenter> {

  constructor(
    private service: CallcenterService,
    private router: Router,
    private  http: JsonHttp
  ) {
  }

  urlFromId(aCallcenterId: string) {
    return this.service.apiUrl + '/' + aCallcenterId;
  }

  resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
    let id = route.params['id'];
    return Observable.create(subject => {
      this.http.get(this.urlFromId(id)).subscribe(data => {
        subject.next(data);
        subject.complete();
      });
    });
  }
}
