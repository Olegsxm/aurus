import { Injectable } from '@angular/core';
import { JsonHttp } from '../../../shared/json-http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/map';
import { AuthService } from '../../../shared/auth.service';
import {window} from "rxjs/operator/window";

export interface Contactcenter {
  id?: string;
  description: string;
  phoneNumber: string;
  ipAddress: string;
  phoneNumberMask: string;
  company?: string;
  settings?: ContactcenterSettings;
}

export interface ISEttings{
  browserCallPrefix: string;
  browserCallTimeout: number;
  callCenterTimeout: number;
  clientCallTimeout: number;
  phoneNumberPattern: string;
  phoneNumberPrefix: string;
  requestSessionTimeout: number;
}

export interface ContactcenterSettings {
  phoneNumberPrefix: string;
  phoneNumberPattern: string;
  browserCallPrefix: string;
  requestSessionTimeout: Number;
  callCenterTimeout: Number;
  clientCallTimeout: Number;
  browserCallTimeout: Number;
}

@Injectable()
export class CallcenterService{

  apiUrl = 'callCenters';

    constructor(
        private http: JsonHttp,
        private authService: AuthService,
    ){}

    getCallcentersList(){
        return this.http.get(this.apiUrl).map(res => res.json())
    }

    getCallCenterSettings(url){
      return this.http.get(url).map(res => res.json()).map(res => {
        return typeof res._embedded == "undefined" ? [] : res._embedded;
      });
    }

    addCallCenter(obj: Contactcenter){

      let CallCenter = {
        company: this.authService.companyUrl,
        description: obj.description,
        ipAddress: obj.ipAddress,
        phoneNumber: obj.phoneNumber,
        phoneNumberMask: obj.phoneNumberMask
      }

      return this.http.post("/admin/callCenters", JSON.stringify(CallCenter)).map(res => res.json());
    }

    addCallCenterSettings(data, settings, cb?){
      let settingsUrl = data._links.self.href
      let settingsData: ISEttings = settings.settings;

      let keys = ['browserCallPrefix','browserCallTimeout','callCenterTimeout','clientCallTimeout','phoneNumberPattern','phoneNumberPrefix', 'requestSessionTimeout'];

      let requestArr = []

      for(let i of keys){

        let obj = {
          key: this.getKeys(i).key,
          value: settingsData[i],
          required: this.getKeys(i).required,
          settingClass: this.getKeys(i).type,
          minValue:'0',
          maxValue:'2147483647',
          regExp: null,
          callCenter: settingsUrl
        };

        let $req = this.http.post('/admin/callCenterSettings', JSON.stringify(obj)).map(res => res.json());

        requestArr.push($req);
      }

      return Observable.forkJoin(requestArr);
    }

    removeCallCenter(url){
      return this.http.delete(url).map(res => res.json())
    }

    getKeys(i){
      let obj = {
        key: null,
        type: null,
        required: null
      };

      switch (i){
        case 'browserCallPrefix': //
          obj.key = 'BROWSER_CALL_PREFIX';
          obj.type = 'STRING';
          obj.required = false;
          break;
        case 'browserCallTimeout':
          obj.key = 'BROWSER_CALL_TIMEOUT';
          obj.type = "NUMBER";
          obj.required = true;
          break;
        case 'callCenterTimeout':
          obj.key = 'CALL_CENTER_CALL_TIMEOUT';
          obj.type = "NUMBER";
          obj.required = true;
          break;
        case 'clientCallTimeout':
          obj.key = 'CLIENT_CALL_TIMEOUT';
          obj.type = "NUMBER";
          obj.required = true;
          break;
        case 'phoneNumberPattern': //
          obj.key = 'PHONE_NUMBER_PATTERN';
          obj.type = "STRING";
          obj.required = true;
          break;
        case 'phoneNumberPrefix': //
          obj.key = 'PHONE_NUMBER_PREFIX';
          obj.type = "STRING";
          obj.required = true;
          break;
        case 'requestSessionTimeout':
          obj.key = 'REQUEST_SESSION_TIMEOUT';
          obj.type = "NUMBER";
          obj.required = true;
          break;
      }

      return obj;
    }


  saveBasicSettings(id,type, value){
    let obj = {}
    obj[type] = value;
    return this.http.patch('/admin/callCenters/'+ id, JSON.stringify(obj)).map(res => {res.json(); obj = {}});
  }

  saveChangedAdditionSettings(callcenterUrl,type:string, value: string, settingUrl){
      let obj = {
        key: this.getKeys(type).key,
        value: value,
        required: this.getKeys(type).required,
        settingClass: this.getKeys(type).type,
        minValue:'0',
        maxValue:'2147483647',
        regExp: null,
        callCenter: callcenterUrl
      }

      return this.http.patch(settingUrl, JSON.stringify(obj)).map(res => res.json());
    }

  save(id, data,ccID, settings){

    let requestArr = [];

    let $one =this.http.patch('/admin/callCenters/'+ id, JSON.stringify(data.basic)).map(res => {res.json()})
      // .subscribe();

    requestArr.push($one);

    let arr = [];

    for(let i in data.settings){
      let obj = {
        data: {
          key: this.getKeys(i).key,
          value: data.settings[i],
          required: this.getKeys(i).required,
          settingClass: this.getKeys(i).type,
          minValue:'0',
          maxValue:'2147483647',
          regExp: null,
          callCenter: ccID
        },
        url: settings[i]._links.self.href
      };

      arr.push(obj);
    }

    for(let i of arr){
      let $req = this.http.patch(i['url'], JSON.stringify(i['data']));
      requestArr.push($req);
    }

    return Observable.forkJoin(requestArr);
  }
}


