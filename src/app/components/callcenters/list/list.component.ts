import { Component, OnInit } from '@angular/core';
import {CallcenterService} from '../services/callcenters.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  providers:[CallcenterService]
})
export class ListComponent implements OnInit {

  constructor(
    private service: CallcenterService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  private displayConfirmWindow: boolean = false;
  private confirmWindowText: string = "call centers";
  private deletedItem = null;

  ngOnInit() {
    this.service.getCallcentersList().subscribe(res => {
      this.items = typeof res._embedded == "undefined" ? [] : res._embedded.callCenters;
      console.warn(this.items)
    })
  }

  items: Array<any> = [];

  add() {
    this.router.navigate(['add'], {relativeTo: this.route});
  }

  edit(item: any){
    let url = item._links.self.href.split('/').pop();
    this.router.navigate(['edit', url], {relativeTo: this.route})
  }

  remove(item){
    this.displayConfirmWindow = true;
    this.deletedItem = item;
    // if(!window.confirm("Delete?")) return;
    // this.service.removeCallCenter(item._links.self.href).subscribe(
    //   res => {},
    //   err => {}
    // )
  }

  deleteItem(event){
    if(event == true){
      this.service.removeCallCenter(this.deletedItem._links.self.href).subscribe(
      res => {
        let index = this.items.indexOf(this.deletedItem);
        this.items.splice(index, 1);
        this.deletedItem = null;
      },
      err => {},
      () => this.displayConfirmWindow = false
    )
    }

    if(event == false){
      this.displayConfirmWindow = false;
      this.deletedItem = null;
    }
  }
}
