import {Component, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import  {CallcenterService, ContactcenterSettings} from '../services/callcenters.service';
import {ErrorPopupComponent} from "../../../shared-components/error-popup/error-popup.component";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  public form: FormGroup;

  @ViewChild(ErrorPopupComponent) errorPopup: ErrorPopupComponent;

  constructor(
    private service: CallcenterService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    let defaults = this.defaults();
    this.form = this.formBuilder.group({
      description: ['', [Validators.required, Validators.minLength(5)]],
      phoneNumber: ['', [Validators.required]],
      ipAddress: ['', [Validators.required, Validators.pattern('^([0-9]{1,3})[.]([0-9]{1,3})[.]([0-9]{1,3})[.]([0-9]{1,3})$')]],
      phoneNumberMask: ['', [Validators.required]],
      settings: this.formBuilder.group({
        phoneNumberPrefix: [defaults.phoneNumberPrefix, [Validators.required]],
        phoneNumberPattern: [defaults.phoneNumberPattern, [Validators.required]],
        browserCallPrefix: [defaults.browserCallPrefix],
        requestSessionTimeout: [defaults.requestSessionTimeout, [Validators.required]],
        callCenterTimeout: [defaults.callCenterTimeout, [Validators.required]],
        clientCallTimeout: [defaults.clientCallTimeout, [Validators.required]],
        browserCallTimeout: [defaults.browserCallTimeout, [Validators.required]]
      })
    });
  }

  save(){
    this.service.addCallCenter(this.form.value)
      .mergeMap(res => this.service.addCallCenterSettings(res, this.form.value))
      .subscribe(
        res => this.router.navigate(['/contactcenters']),
        err => this.errorPopup.showErrorPopup()
      );
  }

  defaults(): ContactcenterSettings {
    return {
      phoneNumberPrefix: '',
      phoneNumberPattern: '',
      browserCallPrefix: '',
      requestSessionTimeout: 0,
      callCenterTimeout: 0,
      clientCallTimeout: 0,
      browserCallTimeout: 0
    };
  }
}
