/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CallcentersComponent } from './callcenters.component';

describe('CallcentersComponent', () => {
  let component: CallcentersComponent;
  let fixture: ComponentFixture<CallcentersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallcentersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallcentersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
