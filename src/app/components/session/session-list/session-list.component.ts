import { Component, OnInit } from '@angular/core';
import {HistoryService} from '../services/history.service';
import { Router, ActivatedRoute } from '@angular/router';
import {Observable} from "rxjs";

@Component({
  selector: 'app-session-list',
  templateUrl: './session-list.component.html',
  styleUrls: ['./session-list.component.scss']
})
export class SessionListComponent implements OnInit {

  sessionList: Array<ISession> | any;
  sessionEventsList;
  totalPages: number = 0;
  totalPagesArray = [];
  currentPage: number = 0;

  pageList: Array<Object> = [];

  constructor(
    private service:HistoryService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.setTotalPages();
    this.mergeLists();
  }

  setTotalPages(){
    this.service.getTotalpages()
      .subscribe(
        res => this.totalPages = res.page.totalPages,
        err => {
          console.error(err);
          this.totalPages = 1;
        },

        () => this.totalPagesArray = Array(this.totalPages).fill(null)
      )
  }

  mergeLists(pageData = null){
    if(pageData == null){
      let $sessions = this.service.getSessionList();

      $sessions.subscribe(
        res => {
          res.map(i => {
            let id = i._links.self.href.split('/').pop();
            let session = i;

            this.service.getSessionEventsListById(id)
              .subscribe(
                res => {
                  let obj = session;
                  obj['entityEvents'] = res;

                  this.pageList.push(obj)
                }
              )

          });
        }
      );

    }

    if(pageData !== null){
      let arr = [];

      pageData.map(i => {
        let id = i._links.self.href.split('/').pop();
        let session = i;


        this.service.getSessionEventsListById(id)
          .subscribe(
            res => {
              let obj = session;
              obj['entityEvents'] = res;
              arr.push(obj);
            },

            err => console.error(err),

            () => this.pageList = arr
          )
      });

    }
  }

  edit(id){
    this.router.navigate(['id', id],
      { relativeTo: this.route });
  }

  getIdFromUrl(url){
    return url.split('/').pop();
  }

  getDate(timestamp){
    return timestamp.split('T')[0];
  }

  getTime(timestamp){
    return timestamp.split('T')[1].split('.')[0];
  }

  getDuration(arr: Array<Object>){

    if(arr.length < 2) return null;

    let result = null;

    if(arr[0]['status'] == "STARTED" && arr[1]['status'] == "CLOSED"){
      let end = +new Date(arr[1]['timestamp']);
      let start = +new Date(arr[0]['timestamp']);

      let seconds = Math.round((end - start) / 1000);

      let h = seconds/3600 ^ 0;
      let m = (seconds - h*3600)/60 ^ 0;
      let s = seconds-h*3600-m*60;

      result = `${h<10?"0"+h:h}:${m<10?"0"+m:m}:${s<10?"0"+s:s}`;
    }

    return result;
  }

  getAgent(users){
    let agent = "";

    users.map( i => {
      if(i.userType == "AGENT"){agent = i.fullName;}
    });

    return agent;
  }

  getType(item){
    return item.type;
  }

  getAction(item){
    return item.type;
  }

  getPage(page: number){
    if(page == this.currentPage || page < 0 || page > this.totalPages) return false;

    this.service.getSessionListOnPage(page)
      .subscribe(
        res => this.mergeLists(res),
        err => console.error(err),
        () => this.currentPage = page
    )
  }

  private deletedItem = null;
  private displayConfirmWindow = false;
  private confirmWindowText = "session";

  delete(item){
    this.displayConfirmWindow = true;
    this.deletedItem = item;
  }

  deleteItem(event){
    if(!event){
      this.deletedItem = null;
      this.displayConfirmWindow = false;
    }

    if(event){

      let id = this.deletedItem._links.self.href;

      // this.service.getSessionEventsListById(this.getIdFromUrl(id))
      //   .subscribe(
      //     res => {
      //       console.clear();
      //       console.warn(res)
      //       let url = res._links.self.href;
      //
      //       this.service.remove(url)
      //         .subscribe(
      //           res => {
      //             this.service.remove(id)
      //               .subscribe(
      //                 res => {
      //                   this.service.getSessionListOnPage(this.currentPage)
      //                     .subscribe(
      //                       res => {
      //                         this.mergeLists(res);
      //                         this.deletedItem = null;
      //                         this.displayConfirmWindow = false;
      //                       },
      //                       err => console.error(err)
      //                     )
      //                 },
      //                 err => console.error(err)
      //               );
      //           },
      //
      //           err => console.error(err)
      //         );
      //     }
      //   );

      this.service.remove(id)
        .subscribe(
          res => {
            this.service.getSessionListOnPage(this.currentPage)
              .subscribe(
                res => {
                  this.mergeLists(res);
                  this.deletedItem = null;
                  this.displayConfirmWindow = false;
                },
                err => console.error(err)
              )
          },
          err => console.error(err)
        );
    }
  }
}


interface ISession {
  links: Object;
  clientPhone: string;
  contents: Array<Object>;
  sessionCode: string;
  status: string;
  timestamp: string;
  type: string;
  users: Array<ILinks>
}

interface ILinks{
  company: {href: string;}
  menuItem: {href: string;}
  self: {href: string;}
  session: {href: string;}
}
