import { Injectable } from '@angular/core';
import { Router, Resolve,
         ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { HistoryService } from '../services/history.service';

export interface ISession{}

@Injectable()
export class HistoryIdResolver implements Resolve<ISession>{
    
    constructor(
        private service: HistoryService,
        private router: Router
    ){

    }

    resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
        return this.service.getSessionItemById(route.params['id']);
    }
}