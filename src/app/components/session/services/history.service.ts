import { Injectable } from '@angular/core';
import {JsonHttp} from '../../../shared/json-http';
//import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class HistoryService{
    private sessionApi = "/admin/sessions/";
    private sessionEntityEventsApi = '/admin/sessionEntityEvents';

    constructor(
        private http: JsonHttp
    ){
    }


    getTotalpages(){
      return this.http.get(this.sessionApi)
        .map(res => res.json())
    }

    getSessionList(){
        return this.http.get(this.sessionApi)
            .map(res => res.json())
            .map(res => res._embedded.sessions)
    }

    getSessionListOnPage(page){
      return this.http.get(this.sessionApi + "?page="+ page)
        .map(res => res.json())
        .map(res => res._embedded.sessions || [])
    }

    getSessionEventsListById(id){
        return this.http.get(this.sessionEntityEventsApi + "/search/byEntityId?entityId=" + id)
        .map(res => res.json())
        .map(res => {return res['_embedded']['sessionEntityEvents']})
        //.map(res => res.filter(i => i.entityId == id))
    }

    getSessionEventsList(){
      return this.http.get(this.sessionEntityEventsApi)
        .map(res => res.json())
        .map(res => typeof res._embedded == "undefined" ? [] : res['_embedded']['sessionEntityEvents'])
    }

    getSessionItemById(id){
        return this.http.get('/admin/sessions/'+ id).map(res => res.json());
    }


    remove(id){
      return this.http.delete(id);
    }
}
