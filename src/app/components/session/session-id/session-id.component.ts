import { Component, OnInit } from '@angular/core';
import { HistoryService } from '../services/history.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-session-id',
  templateUrl: './session-id.component.html',
  styleUrls: ['./session-id.component.scss']
})
export class SessionIdComponent implements OnInit {

  private sessionItem;
  private content;
  private sessionEntityEvents;

  private sessionId;
  private sessionTable;

  constructor(private route: ActivatedRoute, private service: HistoryService) {
    this.sessionId = this.route.params['_value']['id']
  }



  ngOnInit(){
    this.sessionItem = this.route.snapshot.data['topic'];
    this.content = this.sessionItem.contents;

    this.service.getSessionEventsListById(this.sessionId).subscribe(
      res => {
        this.sessionEntityEvents = res;
        this.sessionTable = this.createTable(this.content, this.sessionEntityEvents)
      },
      err => console.error(err),

      () => {
        //    console.warn(this.sessionEntityEvents, "entiti")
        //    console.warn(this.content, 'session');

      }
    )
  }

  createTable(a:Array<any>, b:Array<any>){
    let arr  = a.concat(b);
    return arr.sort( function(a,b):any{return +new Date(a['timestamp']) - +new Date(b['timestamp'])})

  }

  getDate(timestamp: string):string {
    return timestamp.split('T')[1].split('.')[0]
  }

  getOriginal(user){

    if(user == null) return "";

    return user.userType;
  }

  getContent(item){
    let type = item.entityType;

    let result;

    switch (type){
      case "SESSION":result = item.status;break;
      case "MEDIA": result = "media";break;
      case "TEXT": result = item.messageText;break;
      default: result = null;
    }

    return result;
  }
}
