import {Component, OnInit, ViewChild} from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import {IceServiseService} from '../services/ice-servise.service';
import {Router, ActivatedRoute, Route} from "@angular/router";
import {ErrorPopupComponent} from "../../../shared-components/error-popup/error-popup.component";
@Component({
  selector: 'app-ice-add',
  templateUrl: './ice-add.component.html',
  styleUrls: ['./ice-add.component.scss']
})
export class IceAddComponent implements OnInit {

  form:FormGroup;
  formValid: boolean = false;

  @ViewChild(ErrorPopupComponent) errorPopup: ErrorPopupComponent;


  constructor(
    private service: IceServiseService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      type: ['turn', [Validators.required]],
      url: ['', [Validators.required]],
      username: ['',[Validators.required, Validators.minLength(5)]],
      password: ['',[Validators.required, Validators.minLength(5)]],
      repeatPasswod: []
    });

    this.form.valueChanges.subscribe(value => {
      if(this.form.value.type == 'turn'){
        this.formValid = !this.form.invalid;
      }

      if(this.form.value.type == 'stun' && this.form.value.url.length > 0){
        this.formValid = true;
      }
    })
  }

  save(){
    if(!this.formValid) return;

    this.service.save(this.form.value.type, this.form.value)
      .subscribe(
        res => {
          this.router.navigate(['/ice']);
        },
        err => this.errorPopup.showErrorPopup()
      )
  }

}
