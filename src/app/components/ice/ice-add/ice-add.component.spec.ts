/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { IceAddComponent } from './ice-add.component';

describe('IceAddComponent', () => {
  let component: IceAddComponent;
  let fixture: ComponentFixture<IceAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IceAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IceAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
