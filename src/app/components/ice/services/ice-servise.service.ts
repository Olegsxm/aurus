import { Injectable } from '@angular/core';
import {JsonHttp} from "../../../shared/json-http";
import 'rxjs/add/observable/forkJoin';
import {Observable} from "rxjs";
import {AuthService} from "../../../shared/auth.service";

@Injectable()
export class IceServiseService {

  constructor(
    private http: JsonHttp,
    private  auth: AuthService
  ) { }


  getServers(){
    let $turn = this.http.get('/admin/turnServers')
      .map(res => res.json())
      .map(res => typeof res._embedded == 'undefined' ? [] : res._embedded.turnServers);
    let $stun = this.http.get('/admin/stunServers').map(res => res.json())
      .map(res => typeof res._embedded == 'undefined' ? [] : res._embedded.stunServers);

    return Observable.forkJoin([$turn, $stun]);
  }


  save(type,data){
    let model = {
      company: this.auth.companyUrl
    };

    if(type == 'turn'){

      model['url'] = data.url;
      model['username'] = data.username;
      model['password'] = data.password;

      return this.http.post('/admin/turnServers', JSON.stringify(model))
        .map(res => res.json())
    }

    if(type == 'stun'){
      model['url'] = data.url;
      return this.http.post('/admin/stunServers', JSON.stringify(model))
        .map(res => res.json())
    }
  }

  editSave(url,item){
    return this.http.patch(url, JSON.stringify(item));
  }

  remove(item){
    return this.http.delete(item._links.self.href);
  }


  turnResolve(id){
    return this.http.get('/admin/turnServers/' + id).map(res => res.json())
  }

  stunResolve(id){
    return this.http.get('/admin/stunServers/' + id).map(res => res.json());
  }
}
