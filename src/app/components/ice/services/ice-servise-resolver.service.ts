import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import {IceServiseService} from './ice-servise.service';
@Injectable()

export class IceServiseResolverService implements Resolve<any>{

  constructor(
    private router: Router,
    private service: IceServiseService
  ) { }

  resolve(route:ActivatedRouteSnapshot): Observable<any> | Promise<any> | any{
    let id = route.params['id'];
    return Observable.create(subject => {
      this.service.turnResolve(id).subscribe(
        data => {
          subject.next(data);
          subject.complete();
        }
      )
    })
  }
}
