import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {isUndefined} from "util";
import {IceServiseService} from "../services/ice-servise.service";
import {ErrorPopupComponent} from "../../../shared-components/error-popup/error-popup.component";

@Component({
  selector: 'app-ice-edit',
  templateUrl: './ice-edit.component.html',
  styleUrls: ['./ice-edit.component.scss']
})
export class IceEditComponent implements OnInit {

  original;
  form: FormGroup;

  @ViewChild(ErrorPopupComponent) errorPopup:ErrorPopupComponent;

  private Model: IModel = {
    url: null
  };

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private service: IceServiseService,
    private router: Router
  ) { }

  ngOnInit() {
    this.original = this.route.snapshot.data['ice'];

    this.Model.url = this.original.url;

    if(isUndefined(this.original['password'])){
      this.form = this.formBuilder.group({
        url: [this.original.url, [Validators.required]]
      });
    } else {
      this.form = this.formBuilder.group({
        url: [this.original.url],
        username: [this.original.username],
        password: [''],
        repeatPassword: ['']
      });
    }

    this.form.valueChanges.subscribe(
      val => {
        if(!this.form.invalid){
          for(let key in val){
            if(val[key] !== '' && key !== "repeatPassword"){
              this.Model[key] = val[key];
            }
          }
        }
      }
    );
  }


  isTurn(){
    return this.original.username == undefined ? false : true;
  }

  save(){
    if(this.form.invalid) return;

    if(this.Model.password == ''){
      delete this.Model.password;
    }

    let id  = this.original._links.self.href;
    this.service.editSave(id, this.Model).subscribe(
      res => this.router.navigate(['/ice']),
      err => this.errorPopup.showErrorPopup()
    );
  }
}

export interface IModel{
  url:string;
  login?:string;
  password?:string;
}
