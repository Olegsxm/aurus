/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { IceEditComponent } from './ice-edit.component';

describe('IceEditComponent', () => {
  let component: IceEditComponent;
  let fixture: ComponentFixture<IceEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IceEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IceEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
