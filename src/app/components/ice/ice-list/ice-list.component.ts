import { Component, OnInit } from '@angular/core';
import {IceServiseService} from '../services/ice-servise.service';
import {Router, ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-ice-list',
  templateUrl: './ice-list.component.html',
  styleUrls: ['./ice-list.component.scss']
})
export class IceListComponent implements OnInit {

  private items: Array<Object> = [];
  private displayConfirmWindow:boolean = false;
  private confirmWindowText = "server";
  private deletedItem: Object = null;

  constructor(
    private service: IceServiseService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.service.getServers().subscribe(
      res => {
        let turn = res[0].map(i => {
          i['type'] = 'turn';
          this.items.push(i);
        });
        let stun = res[1].map(i => {
          i['type'] = 'stun';
          this.items.push(i);
        });
      }
    );
  }

  add(){
    this.router.navigate(['add'], {relativeTo: this.route});
  }

  edit(item){
    let id = item._links.self.href.split('/').pop();
    this.router.navigate(['edit', item.type, id,], {relativeTo: this.route});
  }

  remove(item){
    this.displayConfirmWindow = true;
    this.deletedItem = item;
  }

  deleteItem($event){

    if($event){
      this.service.remove(this.deletedItem).subscribe(
        res => {
          this.items.splice(this.items.indexOf(this.deletedItem), 1);
          this.displayConfirmWindow = false;
        }
      );
    }

    if(!$event){
      this.deletedItem = null;
      this.displayConfirmWindow = false;
    }
  }
}
