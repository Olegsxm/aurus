/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { IceListComponent } from './ice-list.component';

describe('IceListComponent', () => {
  let component: IceListComponent;
  let fixture: ComponentFixture<IceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IceListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
