import {Route} from "@angular/router";
import {IceComponent} from  '../components/ice/ice.component';
import {IceAddComponent} from  '../components/ice/ice-add/ice-add.component';
import {IceEditComponent} from  '../components/ice/ice-edit/ice-edit.component';
import {IceListComponent} from  '../components/ice/ice-list/ice-list.component';
import {AuthGuard} from "../shared/auth-guard.service";
import {CanDeactivateGuard} from "../shared/can-deactivate-guard.service";
import  {IceServiseResolverService} from '../components/ice/services/ice-servise-resolver.service'
import  {IceStunResolverService} from '../components/ice/services/ice-stun-resolver'


export const iceRoute: Route = {
  path: 'ice',
  component: IceComponent,
  canActivate: [AuthGuard],
  children: [
    {
      path: '',
      pathMatch: 'full',
      component: IceListComponent,
      canDeactivate: [CanDeactivateGuard]
    },
    {
      path: 'edit/turn/:id',
      component: IceEditComponent,
      canDeactivate: [CanDeactivateGuard],
      resolve: {
        ice: IceServiseResolverService
      }
    },

    {
      path: 'edit/stun/:id',
      component: IceEditComponent,
      canDeactivate: [CanDeactivateGuard],
      resolve: {
        ice: IceStunResolverService
      }
    },
    {
      path: 'add',
      pathMatch: 'full',
      component: IceAddComponent
    }
  ]
};
