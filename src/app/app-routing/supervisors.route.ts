import {Route} from "@angular/router";
import {AuthGuard} from "../shared/auth-guard.service";
import {CanDeactivateGuard} from "../shared/can-deactivate-guard.service";
import { PermissionsResolver } from '../shared/permissions.service';

import {SupervisorsComponent} from '../components/supervisors/supervisors.component';
import {SupervisorsAddComponent} from '../components/supervisors/supervisors-add/supervisors-add.component';
import {SupervisorsListComponent} from '../components/supervisors/supervisors-list/supervisors-list.component';
import {SupervisorsEditComponent} from '../components/supervisors/supervisors-edit/supervisors-edit.component';
import {SupervisorsItemResolver} from '../components/supervisors/services/supervisors.item.resolver.service';

export const supervisorsRoute:Route = {
  path: 'supervisors',
  component: SupervisorsComponent,
  canActivate: [AuthGuard],
  children: [
    {
      path: 'add',
      pathMatch: 'full',
      component: SupervisorsAddComponent,
      resolve: {
        permissions: PermissionsResolver
      }
    },
    {
      path: 'edit/:id',
      component: SupervisorsEditComponent,
      canDeactivate: [CanDeactivateGuard],
      resolve: {
        subervisor: SupervisorsItemResolver
      }
    },
    {
      path: '',
      pathMatch: 'full',
      component: SupervisorsListComponent
    },
    {
      path: ':page',
      component: SupervisorsListComponent
    }
  ]
};
