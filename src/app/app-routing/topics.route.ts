import { AuthGuard } from '../shared/auth-guard.service';
import { CanDeactivateGuard } from '../shared/can-deactivate-guard.service';
import {TopicsComponent} from '../components/topics/topics.component';
import {TopicsAddComponent} from '../components/topics/topics-add/topics-add.component';
import {TopicsEditComponent} from '../components/topics/topics-edit/topics-edit.component';
import {TopicsListComponent} from '../components/topics/topics-list/topics-list.component';
import {TopicsItemResolver} from '../components/topics/services/topics.item.resolver.service';
import {Route} from "@angular/router";

export const topicRoute: Route = {
  path: 'topics',
  component: TopicsComponent,
  canActivate: [AuthGuard],
  children: [
    {
      path: 'add',
      pathMatch: 'full',
      component: TopicsAddComponent
    },
    {
      path: 'edit/:id',
      component: TopicsEditComponent,
      canDeactivate: [CanDeactivateGuard],
      resolve: {
        topic: TopicsItemResolver
      }
    },
    {
      path: '',
      pathMatch: 'full',
      component: TopicsListComponent
    },
    {
      path: ':page',
      component: TopicsListComponent
    }
  ]
}
