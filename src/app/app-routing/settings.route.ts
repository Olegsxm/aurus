import {Route} from "@angular/router";
import {SettingsComponent} from '../components/settings/settings.component';
import { PermissionsResolver } from '../shared/permissions.service';
import {AuthGuard} from "../shared/auth-guard.service";

export const settingsRouter: Route = {
  path: 'settings',
  component: SettingsComponent,
  canActivate: [AuthGuard],
  resolve: {
    permissions: PermissionsResolver
  }
}
