import {Route} from "@angular/router";
import {LoginGuard} from "../components/login/login.guard.service";
import {LoginComponent} from '../components/login/login.component';

export const loginRouter: Route = {
  path: 'login',
  component: LoginComponent,
  canActivate: [LoginGuard]
}
