import {Route} from "@angular/router";
import {AgentGroupsComponent} from '../components/agent-groups/agent-groups.component';
import {AgentGroupsAddComponent} from '../components/agent-groups/agent-groups-add/agent-groups-add.component';
import {AgentgroupsListComponent} from '../components/agent-groups/agent-groups-list/agent-groups-list.component';
import {AgentGroupsEditComponent} from '../components/agent-groups/agent-groups-edit/agent-groups-edit.component';
import {AgentgroupsItemResolver} from '../components/agent-groups/services/agentgroups.item.resolver.service'
import { AuthGuard } from '../shared/auth-guard.service';
import { CanDeactivateGuard } from '../shared/can-deactivate-guard.service';
import { PermissionsResolver } from '../shared/permissions.service';

export const agentGroupsRoute: Route =   {
  path: 'agentgroups',
  component: AgentGroupsComponent,
  canActivate: [AuthGuard],
  children: [
    {
      path: 'add',
      pathMatch: 'full',
      component: AgentGroupsAddComponent,
      resolve: {
        permissions: PermissionsResolver
      }
    },
    {
      path: 'edit/:id',
      component: AgentGroupsEditComponent,
      canDeactivate: [CanDeactivateGuard],
      resolve: {
        group: AgentgroupsItemResolver,
        permissions: PermissionsResolver
      }
    },
    {
      path: '',
      pathMatch: 'full',
      component: AgentgroupsListComponent
    },
    {
      path: ':page',
      component: AgentgroupsListComponent
    }
  ]
}
