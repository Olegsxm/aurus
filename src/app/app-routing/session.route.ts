import {Route} from "@angular/router";
import {SessionComponent} from '../components/session/session.component';
import {SessionIdComponent} from '../components/session/session-id/session-id.component';
import {SessionListComponent} from '../components/session/session-list/session-list.component';
import {AuthGuard} from "../shared/auth-guard.service";
import {CanDeactivateGuard} from "../shared/can-deactivate-guard.service";
import {HistoryIdResolver} from '../components/session/services/history.id.resolver';

export const sessionRoute:Route = {
  path: 'history',
  component: SessionComponent,
  canActivate: [AuthGuard],
  children: [
    {
      path: 'id/:id',
      component: SessionIdComponent,
      canDeactivate: [CanDeactivateGuard],
      resolve: {
        topic: HistoryIdResolver
      }
    },
    {
      path: '',
      pathMatch: 'full',
      component: SessionListComponent
    }
  ]
};
