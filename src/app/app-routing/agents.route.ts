import {Route} from "@angular/router";
import { AuthGuard } from '../shared/auth-guard.service';
import { CanDeactivateGuard } from '../shared/can-deactivate-guard.service';
import {AgentsComponent} from '../components/agents/agents.component';
import {AgentsAddComponent} from '../components/agents/agents-add/agents-add.component';
import {AgentsEditComponent} from '../components/agents/agents-edit/agents-edit.component';
import {AgentsListComponent} from '../components/agents/agents-list/agents-list.component';
import {AgentsItemResolver} from '../components/agents/services/agents.item.resolver.service';
import { AgentgroupsResolver } from '../components/agent-groups/services/agentgroups.service';

export const agentsRoute:Route = {
  path: 'agents',
  component: AgentsComponent,
  canActivate: [AuthGuard],
  children: [
    {
      path: 'add',
      pathMatch: 'full',
      component: AgentsAddComponent,
      resolve: {
        groups: AgentgroupsResolver
      }
    },
    {
      path: 'edit/:id',
      component: AgentsEditComponent,
      canDeactivate: [CanDeactivateGuard],
      resolve: {
        agent: AgentsItemResolver,
        groups: AgentgroupsResolver
      }
    },
    {
      path: '',
      pathMatch: 'full',
      component: AgentsListComponent
    },
    {
      path: ':page',
      component: AgentsListComponent
    }
  ]
}
