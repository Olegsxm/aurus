import {Route} from "@angular/router";
import {CallcentersComponent} from "../components/callcenters/callcenters.component";
import {AddComponent} from "../components/callcenters/add/add.component";
import {ListComponent} from "../components/callcenters/list/list.component";
import {EditComponent} from "../components/callcenters/edit/edit.component";
import {ContactcentersItemResolver} from "../components/callcenters/services/callcenter.resolve";
import {AuthGuard} from "../shared/auth-guard.service";
import {CanDeactivateGuard} from "../shared/can-deactivate-guard.service";

export const callcentersRouter: Route = {
  path: 'contactcenters',
  component: CallcentersComponent,
  canActivate: [AuthGuard],
  children: [
    {
      path: 'add',
      pathMatch: 'full',
      component: AddComponent
    },
    {
      path: 'edit/:id',
      component: EditComponent,
      canDeactivate: [CanDeactivateGuard],
      resolve: {
        callcenter: ContactcentersItemResolver
      }
    },
    {
      path: '',
      pathMatch: 'full',
      component: ListComponent
    },
    //   {
    //     path: ':page',
    //     component: ContactcentersListComponent
    //   }
  ]
}
