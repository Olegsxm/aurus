import {Route} from "@angular/router";
import { AdminComponent } from '../components/admin/admin.component';
import {AuthGuard} from "../shared/auth-guard.service";
import {GreetingComponent} from '../components/admin/greeting/greeting.component';
import {ChangePasswordComponent} from '../components/admin/change-password/change-password.component';
import {CanDeactivateGuard} from "../shared/can-deactivate-guard.service";

export const adminRoute:Route = {
  path: 'admin',
  component: AdminComponent,
  canActivate: [AuthGuard],
  children: [
    {
      path: '',
      pathMatch: 'full',
      component: GreetingComponent,
      canDeactivate: [CanDeactivateGuard]
    },

    {
      path: 'changepasswors',
      pathMatch:'full',
      component: ChangePasswordComponent,
      canDeactivate: [CanDeactivateGuard]
    }
  ]
};
