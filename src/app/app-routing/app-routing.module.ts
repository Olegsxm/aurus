import { NgModule } from '@angular/core';
import {RouterModule, Routes, Route} from '@angular/router';
import {AuthGuard} from "../shared/auth-guard.service";

import {settingsRouter} from "./settings.route";
import {loginRouter} from "./login.route";
import {callcentersRouter} from "./callcenters.route";
import {agentGroupsRoute} from "./agentGroups.route";
import {topicRoute} from "./topics.route";
import {agentsRoute} from "./agents.route";
import {sessionRoute} from "./session.route";
import {supervisorsRoute} from "./supervisors.route";
import {adminRoute} from "./admin.route";
import {iceRoute} from "./ice.route";



const routes: Routes = [
  {
    path: '',
    pathMatch:'full',
    redirectTo: '/admin'
  },
  settingsRouter,
  loginRouter,
  callcentersRouter,
  agentGroupsRoute,
  topicRoute,
  agentsRoute,
  sessionRoute,
  supervisorsRoute,
  adminRoute,
  iceRoute
]

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }



