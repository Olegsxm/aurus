import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import {DropdownComponent} from './shared-components/dropdown/dropdown.component';

import { SettingsComponent } from './components/settings/settings.component';
import {AppRoutingModule} from "./app-routing/app-routing.module";
import {SharedModule} from "./shared/shared.module";
import { LoginComponent } from './components/login/login.component';
import {LoginGuard} from "./components/login/login.guard.service";
import EqualTextValidator from '../../node_modules/angular2-text-equality-validator/angular2-text-equality-validator';
import { CallcentersComponent } from './components/callcenters/callcenters.component';

import {CallcenterService} from "./components/callcenters/services/callcenters.service";
import {ContactcentersItemResolver} from "./components/callcenters/services/callcenter.resolve";
import { ListComponent } from './components/callcenters/list/list.component';
import { AddComponent } from './components/callcenters/add/add.component';
import { EditComponent } from './components/callcenters/edit/edit.component';
import { PermissionsComponent } from './components/settings/permissions/permissions.component';
import { AgentGroupsComponent } from './components/agent-groups/agent-groups.component';
import { AgentgroupsListComponent } from './components/agent-groups/agent-groups-list/agent-groups-list.component';
import { AgentGroupsAddComponent } from './components/agent-groups/agent-groups-add/agent-groups-add.component';
import { AgentGroupsEditComponent } from './components/agent-groups/agent-groups-edit/agent-groups-edit.component';
import { AgentGroupsPermissionsComponent } from './components/agent-groups/agent-groups-permissions/agent-groups-permissions.component';

import {AgentgroupsService} from './components/agent-groups/services/agentgroups.service';
import {AgentgroupsItemResolver} from './components/agent-groups/services/agentgroups.item.resolver.service';
import { TopicsComponent } from './components/topics/topics.component';
import { TopicsListComponent } from './components/topics/topics-list/topics-list.component';
import { TopicsEditComponent } from './components/topics/topics-edit/topics-edit.component';
import { TopicsAddComponent } from './components/topics/topics-add/topics-add.component';
import {TopicsItemResolver} from './components/topics/services/topics.item.resolver.service';
import {TopicsService} from './components/topics/services/topics.service';
import { AgentsComponent } from './components/agents/agents.component';
import { AgentsAddComponent } from './components/agents/agents-add/agents-add.component';
import { AgentsEditComponent } from './components/agents/agents-edit/agents-edit.component';
import { AgentsListComponent } from './components/agents/agents-list/agents-list.component';
import { AgentsGroupsComponent } from './components/agents/agents-groups/agents-groups.component';
import { AgentsService } from './components/agents/services/agents.service';
import { AgentsItemResolver } from './components/agents/services/agents.item.resolver.service';
import { AgentgroupsResolver } from './components/agent-groups/services/agentgroups.service';
import { SessionComponent } from './components/session/session.component';
import { SessionIdComponent } from './components/session/session-id/session-id.component';
import { SessionListComponent } from './components/session/session-list/session-list.component';
import {HistoryIdResolver} from './components/session/services/history.id.resolver';
import {HistoryService} from './components/session/services/history.service';
import { SupervisorsComponent } from './components/supervisors/supervisors.component';
import { SupervisorsAddComponent } from './components/supervisors/supervisors-add/supervisors-add.component';
import { SupervisorsEditComponent } from './components/supervisors/supervisors-edit/supervisors-edit.component';
import { SupervisorsListComponent } from './components/supervisors/supervisors-list/supervisors-list.component';
import { SupervisorsItemResolver } from './components/supervisors/services/supervisors.item.resolver.service';
import { SupervisorsService } from './components/supervisors/services/supervisors.service';
import { ConfirmWindowComponent } from './shared-components/confirm-window/confirm-window.component';
import { AdminComponent } from './components/admin/admin.component';
import { GreetingComponent } from './components/admin/greeting/greeting.component';
import { ChangePasswordComponent } from './components/admin/change-password/change-password.component';
import { IceComponent } from './components/ice/ice.component';
import { IceListComponent } from './components/ice/ice-list/ice-list.component';
import { IceEditComponent } from './components/ice/ice-edit/ice-edit.component';
import { IceAddComponent } from './components/ice/ice-add/ice-add.component';
import { IceServiseService } from './components/ice/services/ice-servise.service';
import { IceServiseResolverService } from './components/ice/services/ice-servise-resolver.service';
import { IceStunResolverService } from './components/ice/services/ice-stun-resolver';
import { ErrorPopupComponent } from './shared-components/error-popup/error-popup.component';

@NgModule({
  declarations: [
    AppComponent,
    DropdownComponent,
    EqualTextValidator,
    SettingsComponent,
    LoginComponent,
    PermissionsComponent,
    CallcentersComponent,
    ListComponent,
    AddComponent,
    EditComponent,
    AgentGroupsComponent,
    AgentgroupsListComponent,
    AgentGroupsAddComponent,
    AgentGroupsEditComponent,
    AgentGroupsPermissionsComponent,
    TopicsComponent,
    TopicsListComponent,
    TopicsEditComponent,
    TopicsAddComponent,
    AgentsComponent,
    AgentsAddComponent,
    AgentsEditComponent,
    AgentsListComponent,
    AgentsGroupsComponent,
    SessionComponent,
    SessionIdComponent,
    SessionListComponent,
    SupervisorsComponent,
    SupervisorsAddComponent,
    SupervisorsEditComponent,
    SupervisorsListComponent,
    ConfirmWindowComponent,
    AdminComponent,
    GreetingComponent,
    ChangePasswordComponent,
    IceComponent,
    IceListComponent,
    IceEditComponent,
    IceAddComponent,
    ErrorPopupComponent
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    SharedModule
  ],
  providers: [
    LoginGuard,CallcenterService,
    ContactcentersItemResolver,
    AgentgroupsService,
    AgentgroupsItemResolver,
    TopicsItemResolver,
    TopicsService,
    AgentsService,
    AgentsItemResolver,
    AgentgroupsResolver,
    HistoryIdResolver,
    HistoryService,
    SupervisorsItemResolver,
    SupervisorsService,
    IceServiseService,
    IceServiseResolverService,
    IceStunResolverService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
