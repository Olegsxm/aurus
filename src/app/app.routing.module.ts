import { NgModule }     from '@angular/core';
import { RouterModule, ExtraOptions } from '@angular/router';

const config: ExtraOptions = {
  enableTracing: true
};

@NgModule({
  imports: [
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: '/settings',
        pathMatch: 'full',
      }
    ], config)
  ],
  exports: [
    RouterModule
  ],
  providers: []
})
export class AppRoutingModule {}

/*
const appRoutes: Routes = [
  {
    path: '',
    component: MainComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'settings',
    component: SettingsComponent,
    canActivate: [AuthGuard],
    resolve: {
      settings: SettingsResolver,
      permissions: PermissionsResolver
    }
  },
  {
    path: 'topics',
    component: TopicsComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', component: TopicsListComponent },
      { path: ':page', component: TopicsComponent },
      { path: ':add', component: TopicsEditComponent },
      { path: 'edit/:id', component: TopicsEditComponent },
    ]
  },
  {
    path: 'agents',
    component: AgentsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'supervisors',
    component: SupervisorsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'history',
    component: HistoryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '**',
    redirectTo: ''
  }
];

const config: ExtraOptions = {
  enableTracing: true
};

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes, config);
*/
