import { RcAdminCcPage } from './app.po';

describe('rc-admin-cc App', function() {
  let page: RcAdminCcPage;

  beforeEach(() => {
    page = new RcAdminCcPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
